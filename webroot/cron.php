<?php
use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\CreditCardToken;
use PayPal\Api\Details;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;  
include('db.php');
include('../vendor/PayPal-PHP-SDK/autoload.php');
$apiContext = new \PayPal\Rest\ApiContext(
    new \PayPal\Auth\OAuthTokenCredential(
    CLIENT_ID,     // ClientID
    CLIENT_SECRET_KEY      // ClientSecret
)
);

$to = "karunadri@natitsolved.com";
$subject = "My subject";
$txt = "Hello world!";
$headers = "From: webmaster@example.com" . "\r\n" .
"CC: somebodyelse@example.com";
mail($to,$subject,$txt,$headers);
$sql="select * from `users` where subscription_end<='". gmdate("Y-m-d")."' and  paypal_id!='' LIMIT 1" ;
$sub_sql="select * from `subscriptions` where id=1" ;

try {
    $sth = $dbh->prepare($sql);
    $sth->execute();
    $user = $sth->fetch(PDO::FETCH_ASSOC);
    
    $sth = $dbh->prepare($sub_sql);
    $sth->execute();
    $subscription = $sth->fetch(PDO::FETCH_ASSOC);
    
    $creditCardToken = new CreditCardToken();
    $creditCardToken->setCreditCardId($user["paypal_id"]);
    // ### FundingInstrument
    // A resource representing a Payer's funding instrument.
    // For stored credit card payments, set the CreditCardToken
    // field on this object.
    $fi = new FundingInstrument();
    $fi->setCreditCardToken($creditCardToken);
    // ### Payer
    // A resource representing a Payer that funds a payment
    // For stored credit card payments, set payment method
    // to 'credit_card'.
    $payer = new Payer();
    $payer->setPaymentMethod("credit_card")
        ->setFundingInstruments(array($fi));
    // ### Itemized information
    // (Optional) Lets you specify item wise
    // information
    $qty=1;
    

    // ### Amount
    // Lets you specify a payment amount.
    // You can also specify additional details
    // such as shipping, tax.
    $net_total=$subscription["price"];
    $amount = new Amount();
    $amount->setCurrency("USD")
        ->setTotal($net_total)
        ;
    // ### Transaction
    // A transaction defines the contract of a
    // payment - what is the payment for and who
    // is fulfilling it. 
    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setDescription("Yearly Subscription")
        ->setInvoiceNumber(uniqid());
    // ### Payment
    // A Payment Resource; create one using
    // the above types and intent set to 'sale'
    $payment = new Payment();
    $payment->setIntent("sale")
        ->setPayer($payer)
        ->setTransactions(array($transaction));
    // For Sample Purposes Only.
    $request = clone $payment;
    try 
    {
     $transaction= json_decode($payment->create($apiContext)); 
     $subscription_start=gmdate("Y-m-d");
     $subscription_end=date('Y-m-d', strtotime($subscription_start. ' + 365 days')) ;
     $sql="update users set subscription_start='".$subscription_start."',subscription_end='".$subscription_end."' where id='".$user["id"]."'";
     $sth = $dbh->prepare($sql);
     $sth->execute(); 
     
     $sql="INSERT INTO `subscribers` (`user_id`, `subscribe_date`, `transaction_id`) VALUES ('".$user["id"]."', '". gmdate("Y-m-d H:i:s")."', '".$transaction->id."')";
     $sth = $dbh->prepare($sql);
     $sth->execute();   
    } catch (Exception $ex) {
        echo "<pre>";
        print_r($ex);
        
    } 
} catch (Exception $ex) {
    print_r($ex);   
}


?>