<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
<script>
  $(document).ready(function() {
      $('#summernote').summernote();
      $('#editor1').summernote({
          height: 300,                 // set editor height
          width: 500,
          minHeight: null,             // set minimum height of editor
          maxHeight: null,             // set maximum height of editor
          focus: true,                  // set focus to editable area after initializing summernote
      });
  });
</script>

<div id="content">
    <div class="inner">
        <div class="row">
            <div class="col-lg-12">
                <h1 > Treatment List </h1>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <header>
                        <div class="icons"><i class="icon-th-large"></i></div>
                        <h5>Treatment List</h5>
                        <div class="toolbar">
                            <ul class="nav">
                                <li style="margin-right:15px">
                                    <div class="btn-group"> 
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <div id="collapseOne" class="accordion-body collapse in body">
                        <div class="col-sm-12">
                            <div class="row">                               
                                <div class="form-group">
                                    <label class="control-label col-lg-2">  Question : </label>
                                    <?php echo '<div class="col-lg-10">' . $question['question'] . '</div>'; ?>
                                </div>
                                <hr />
                                <div class="form-group"> 
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th style="width:5%">#</th>
                                                        <th style="width:25%">Treatment</th>
                                                        <th style="width:18%">Added</th>
<!--                                                        <th style="width:18%">Updated</th>-->
                                                        <th style="width:10%">Status</th>
                                                        <th style="width:10%">Answer</th>
                                                        <th class="actions" style="width:22%"><?php echo __('Actions') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    //pr($questions);
                                                    if (!empty($questions)) { ?>        
                                                        <?php $ik = 1; foreach ($questions as $dt): ?>
                                                            <tr>
                                                                <td><?php echo $this->Number->format($ik) ?></td>
                                                                <td><?php echo h($dt['Treatments']['name']) ?></td>
                                                                <td><?php echo h($this->requestAction('admin/users/change_datetimeformat/'.strtotime($dt['added_on']))) ?></td>
<!--                                                                <td><?php 
                                                                
                                                                echo (!empty(($dt['updated_on']))?($this->requestAction('admin/users/change_datetimeformat/'.strtotime($dt['updated_on']))):'N/A'); ?></td>-->
                                                                <td>
                                                                    <?php
                                                                        if(!empty($dt['valcheck']))
                                                                        {
                                                                            echo "Yes";
                                                                        }
                                                                        else {
                                                                            echo "No";
                                                                        }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                        if(!empty($dt['is_active']))
                                                                        {
                                                                            echo "Active";
                                                                        }
                                                                        else {
                                                                            echo "Inactive";
                                                                        }
                                                                    ?>
                                                                </td>
                                                                <td class="actions">        
                                                                    <a href="<?php echo $this->Url->build(["action" => "treatmentdelete", $dt['qid'], $dt['id']]); ?>" onclick="return confirm('Are you sure, you want to delete?');"> <button class="btn btn-danger btn-xs"><i class="icon-remove icon-white"></i> Delete</button> </a>  
                                                                    <?php if ($dt->is_active == 1) { ?>
                                                                    <a href="<?php echo $this->Url->build(["action" => "treatmentstatus", $dt['qid'], $dt->id, '0']); ?>"> <button class="btn btn-info btn-xs" style='margin-top:4px;'><i class="icon-thumbs-down"></i> Suspend</button> </a>
                                                                <?php } else if ($dt->is_active == 0) { ?>
                                                                    <a href="<?php echo $this->Url->build(["action" => "treatmentstatus", $dt['qid'], $dt->id, '1']); ?>"> <button class="btn btn-success btn-xs" style='margin-top:4px;'><i class="icon-thumbs-up"></i> Active</button> </a>
                                                                <?php } ?>  
                                                                </td>                
                                                            </tr>
                                                            <?php $ik ++; endforeach; ?>
                                                            <?php } else { ?> 
                                                            <tr>
                                                                <td colspan="6">No Question Exist.</td>              
                                                            </tr>
                                                            <?php } ?>        
                                                </tbody>
                                            </table>
                                        </div>  
                                    </div>
                                </div>                                
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>        

        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <header>
                        <div class="icons"><i class="icon-th-large"></i></div>
                        <h5>Add Question</h5>
                        <div class="toolbar">
                            <ul class="nav">
                                <li style="margin-right:15px">
                                    <div class="btn-group"> 
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <div id="collapseOne" class="accordion-body collapse in body">
                        <div class="col-sm-6">
                            <div class="row">
                                <?php echo $this->Form->create($treatmentQuestions, ['class' => 'form-horizontal', 'id' => 'tq-validate']); ?>
                                <input type="hidden" name="qid" id="qid" value="<?php echo $question['id']; ?>">

                                <div class="form-group">
                                    <label class="control-label col-lg-4">  Choose Treatment </label>
                                    <?php echo '<div class="col-lg-8">' . $this->Form->input('tid', ['class' => 'form-control chzn-select', 'onchange'=>'myfunc(this.value)', 'label' => false, 'type' => 'select','multiple' => false,'options' => $treatments, 'empty' => true, 'style' => 'width:500px']) . '</div>'; ?>
                                </div>                                
                                <?php $answer = ['' => 'Choose Answer', 1 => 'Yes', 0 => 'No']; ?>
                                <div class="form-group" id="yesno" style="display: block">
                                    <label class="control-label col-lg-4">  Answer </label>
                                    <?php echo '<div class="col-lg-8">' . $this->Form->input('valcheck', ['class' => 'form-control chzn-select', 'label' => false, 'type' => 'select','multiple' => false,'options' => $answer, 'empty' => "Choose Answer", 'default'=>'', 'style' => 'width:500px']) . '</div>'; ?>
                                </div>                                 
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-lg-4">  Is Active ? </label>
                                    <?php echo '<div class="col-lg-8">' . $this->Form->input('is_active', array('label' => false, 'type' => 'checkbox')) . '</div>'; ?>
                                </div>                                

                                <label class="control-label col-lg-4"></label> 
                                <div class="col-lg-8" style="text-align:left;"> 
                                    <input type="submit" name="submit" value="Add Treatment" class="btn btn-primary" />
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function myfunc(dt){      
        jQuery.ajax({           
            type:"post",
            //dataType:"json", 
            url: "<?php echo $this->Url->build('/'); ?>admin/treatments/checkqtype/",
            data: {id: dt},
            success: function(data) {
                if(data == 1){
                    $('#yesno').css('display','block');
                } else {
                    $('#yesno').css('display','none');
                }
            }
        });        
        
        
    }

</script>