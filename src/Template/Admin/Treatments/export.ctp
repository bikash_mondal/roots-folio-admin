<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row">
            <div class="col-lg-12">
                <h1 >Medicines Management </h1>
            </div>
        </div>
        <hr />
        <div class="row">          
            <div class="col-lg-12">
                <div class="box">
                    <header>
                        <div class="icons"><i class="icon-th-large"></i></div>
                        <h5>Export Medicines</h5>
                        <div class="toolbar">
                            <ul class="nav">
                                <li style="margin-right:15px">
                                    <div class="btn-group"> 

                                    </div>
                                </li>

                            </ul>
                        </div>
                    </header>
                    <div id="collapseOne" class="accordion-body collapse in body">
                        <div class="col-sm-6">

                            <div class="row">
				  <?php echo $this->Form->create('Filter', array('class'=>'form-horizontal','type'=>'POST'));?>

                                <div class="form-group">
                                    <label class="control-label col-lg-4">Treatment:</label>
                                    <div class="col-lg-8">
                                        <?php
                                        array_unshift($treatments,'All Treatment');
                                        echo $this->Form->input('treatment_id', array('class'=>'form-control','label'=>false,'div'=>false,'options'=>$treatments,'style'=>'width:200px;','default'=>!empty($_REQUEST['treatment_id'])?$_REQUEST['treatment_id']:'','required' => 'required')); ?>
                                    </div>
                                </div>
                                
                                <label class="control-label col-lg-4"></label>
                                <div class="col-lg-8" style="text-align:left;"> 
                                    <input type="submit" name="export_button" value="Export" class="btn btn-primary" />
                                </div>
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-12">
                <div class="box">
                    <header>
                        <div class="icons"><i class="icon-th-large"></i></div>
                        <h5>Upload Medicines</h5>
                        <div class="toolbar">
                            <ul class="nav">
                                <li style="margin-right:15px">
                                    <div class="btn-group"> 

                                    </div>
                                </li>

                            </ul>
                        </div>
                    </header>
                    <div id="collapseOne" class="accordion-body collapse in body">
                        <div class="col-sm-6">

                            <div class="row">
				  <?php echo $this->Form->create('Pill', array('class'=>'form-horizontal','type'=>'POST', 'enctype' => 'multipart/form-data'));?>

                                <div class="form-group">
                                    <label class="control-label col-lg-4">Medicine CSV:</label>
                                    <div class="col-lg-8">
                                        <?php echo $this->Form->input('medicine_csv', array('label'=>false,'empty'=>'Choose Treatments','div'=>false,'type' => 'file','style'=>'width:200px;','required' => 'required')); ?>
                                        <small style="color:red"> Its mandatory to upload a valid CSV downloaded from above.</small>
                                    </div>
                                </div>
                                
                                <label class="control-label col-lg-4"></label>
                                <div class="col-lg-8" style="text-align:left;"> 
                                    <input type="submit" name="submit" value="Upload" class="btn btn-primary" />
                                </div>
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>        
     </div>
</div>
<!--END PAGE CONTENT --> 
