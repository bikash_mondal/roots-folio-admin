<?php //pr($sitesettings); exit; ?>
<div id="content">
    <div class="inner">
        <div class="row">
            <div class="col-lg-12">
                <h1 > Picing & Subscribers Detail </h1>
            </div>
        </div>
        <hr />
	  <?php //echo $this->Flash->render('success') ?>
	  <?php //echo $this->Flash->render('error') ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <header>
                        <div class="icons"><i class="icon-th-large"></i></div>
                        <h5> Edit Site SEO Detail </h5>
                        <div class="toolbar">
                            <ul class="nav">
                                <li style="margin-right:15px">
                                    <div class="btn-group"> 
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <div id="collapseOne" class="accordion-body collapse in body">
                        <div class="col-sm-12">

                            <div class="row">
				  <?php echo $this->Form->create($sitesettings,['class' => 'form-horizontal', 'id' => 'siteset-validate', 'type' => 'post', 'enctype' => 'multipart/form-data']);?>

                                <div class="form-group">
                                    <label class="control-label col-lg-3">Price Title</label>
                                    <div class="col-lg-9">
                                        <input type="text" id="site_meta_title" name="title" class="form-control" value="<?php echo $sitesettings->title;?>"/>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-lg-3" for="autosize">Price Description</label>
                                    <div class="col-lg-9">
                                        <textarea id="site_meta_description" name="description" class="form-control" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 200px;"><?php echo $sitesettings->description;?></textarea>
                                    </div>
                                </div> 
                                 <div class="form-group">
                                    <label class="control-label col-lg-3">Product Title</label>
                                    <div class="col-lg-9">
                                        <input type="text" id="site_meta_title" name="product_name" class="form-control" value="<?php echo $sitesettings->product_name;?>"/>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Subscription Title</label>
                                    <div class="col-lg-9">
                                        <input type="text" id="site_meta_title" name="subscription_name" class="form-control" value="<?php echo $sitesettings->subscription_name;?>"/> 
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-lg-3">Subscribers Title</label>
                                    <div class="col-lg-9">
                                        <input type="text" id="site_meta_title" name="subscribe_head" class="form-control" value="<?php echo $sitesettings->subscribe_head;?>"/>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-lg-3" for="autosize">Subscribe Description</label>
                                    <div class="col-lg-9">
                                        <textarea id="site_meta_description" name="subscribe_content" class="form-control" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 200px;"><?php echo $sitesettings->subscribe_content;?></textarea>
                                    </div>
                                </div> 
                                
                                                          
<!--                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="autosize">Google Analytics Code</label>
                                    <div class="col-lg-9">
                                        <textarea id="google_analytics" name="google_analytics" class="form-control" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 200px;"><?php echo $sitesettings->google_analytics;?></textarea>
                                    </div>
                                </div>-->

                                <label class="control-label col-lg-3"></label>
                                <div class="col-lg-9" style="text-align:left;"> 
                                    <input type="submit" value="Update" class="btn btn-primary" />
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>