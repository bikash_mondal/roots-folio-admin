<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row">
            <div class="col-lg-12">
                <h1 > Orders </h1>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <?php echo $this->Form->create('Filter', array('class'=>'form-inline','type'=>'get'));?>
                        <div class="form-group">
                          <label for="email">Zip:</label>
                          <?php echo $this->Form->input('zip', array('class'=>'form-control','label'=>false,'placeholder'=>'Search By Zip','div'=>false,'value'=>!empty($_REQUEST['zip'])?$_REQUEST['zip']:'')); ?>
                        </div>
                        <button type="submit" class="btn btn-success" style='margin-top:27px;margin-bottom:6px;'>Search</button>
                        <button type="button" class="btn btn-success" style="margin-top:27px;margin-bottom:6px;" onclick="resetForm()">Clear Search</button>
                    <?php echo $this->Form->end();?>
                    <header>
                        <div class="icons"><i class="icon-th-large"></i></div>
                        <h5> Orders waiting for ship</h5>
                        <div class="toolbar">
                            <ul class="nav">
                                <li style="margin-right:15px">
                                    <div class="btn-group" style=" margin-top: 8px">
                                        <form method="post" target="_blank">
                                            <button type="submit" name="export" class="btn btn-info btn-xs" value="export"><i class="icon-download icon-white"></i>Export</button>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </header>
                    <div id="collapseOne" class="accordion-body collapse in body">
                        <div class="col-sm-12">
                            <div class="row">                               
                                <div class="form-group"> 
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th><?php echo $this->Paginator->sort('Sl No') ?></th>
                                                        <th><?php echo $this->Paginator->sort('Order From') ?></th>
                                                        <th><?php echo $this->Paginator->sort('email') ?></th>
                                                        <th><?php echo $this->Paginator->sort('contact','Phone No.') ?></th>
                                                        <th><?php echo $this->Paginator->sort('shipping_code','Zip') ?></th>
                                                        <th><?php echo $this->Paginator->sort('shipping_city','City') ?></th>
                                                        <th><?php echo $this->Paginator->sort('Order Date') ?></th>
                                                        <th class="actions"><?php echo __('Action') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (!empty($orders)) { ?>
                                                        <?php $ik = 1;
                                                        foreach ($orders as $order): ?>
                                                            <tr>
                                                                <td><?php echo $ik ?></td>
                                                                <td><?php echo $order->name; ?></td>
                                                                <td><?php echo $order->email; ?></td>
                                                                <td><?php echo $order->contact; ?></td>
                                                                <td><?php echo $order->shipping_code; ?></td>
                                                                <td><?php echo $order->shipping_city; ?></td>
                                                                <td><?php echo date('d F Y', strtotime($order->date)); ?></td>
                                                                <td class="actions">
                                                                    <a href="<?php echo $this->Url->build(["action" => "view", $order->transaction_id]); ?>"> <button class="btn btn-info btn-xs"><i class="icon-eye-open"></i> View</button> </a>
                                                                    <a href="<?php echo $this->Url->build(["action" => "change_shipping", $order->id,'S']); ?>"> <button class="btn btn-success btn-xs">Ship</button> </a>
                                                                    <a href="<?php echo $this->Url->build(["action" => "change_shipping", $order->id,'R']); ?>"> <button class="btn btn-danger btn-xs" onclick="return confirm('are you sure you want to cancel this order?');">Cancel</button> </a>
                                                                </td>
                                                            </tr>
                                                            <?php $ik++;
                                                        endforeach; ?>
                                                        <?php } else { ?>
                                                        <tr>
                                                            <td colspan="5"> No Data Exist </td>
                                                        </tr>                       
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                            <div class="paginator">
                                                <ul class="pagination">
                                                    <?php echo $this->Paginator->first(__('<< First', true), array('class' => 'number-first')); ?>
                                                    <?php echo $this->Paginator->numbers(array('class' => 'numbers', 'first' => false, 'last' => false)); ?>
                                                    <?php echo $this->Paginator->last(__('>> Last', true), array('class' => 'number-end')); ?>
                                                </ul>
                                                <p><?php //echo $this->Paginator->counter()  ?></p>
                                            </div>
                                        </div>  
                                    </div>
                                </div>                                
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
<script>
    function resetForm()
{
    window.location.href="<?php echo $this->Url->build(["action" => "index"]); ?>";

  
}
</script>
<!--END PAGE CONTENT -->
