<!--PAGE CONTENT -->
<div id="content">
    <div class="inner">
        <div class="row">
            <div class="col-lg-12">
                <h1 > Order Detail </h1>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="my-order-tab">
                        <header class="main-content-header" style="padding-left: 10px;">
                            <h2 class="my-orders"> Delivery Address </h2>
                        </header>
                        <div class="order-conteainer">
                            <div class="order-detail-content">
                                
                                    <div class="col-md-12">
                                        <table class="table">
                                            <tr>
                                                <td><b>Patient Name:</b></td>
                                                <td> <?php echo $orderExist[0]->name ?> </td>
                                            </tr>
                                            <tr>
                                                <td><b>Patient Email:</b></td>
                                                <td><?php echo $orderExist[0]->email ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Patient Contact:</b></td>
                                                <td> <?php echo $orderExist[0]->contact ?> </td>
                                            </tr>
                                            <tr>
                                                <td><b>Patient Address:</b></td>
                                                <td> <?php echo $orderExist[0]->shipping_address ?> </td>
                                            </tr>
                                            <tr>
                                                <td><b>Patient City:</b></td>
                                                <td> <?php echo $orderExist[0]->shipping_city ?> </td>
                                            </tr>
                                            <tr>
                                                <td><b>Patient Country:</b></td>
                                                <td><?php echo $orderExist[0]->shipping_country ?> </td>
                                            </tr>
                                             <tr>
                                                <td><b>Order Date:</b></td>
                                                <td> <?php echo date('d F Y', strtotime($orderExist[0]->date)); ?> </td>
                                            </tr>
                                            <tr>
                                                <td><b>Shipped Date:</b></td>
                                                <td> <?php
                                                if(!empty($orderExist[0]->shipped_date) && $orderExist[0]->shipped_date!='0000-00-00 00:00:00')
                                                {
                                                    if($orderExist[0]->shipping_status=='P')
                                                    {
                                                        echo "Not shipped yet.";
                                                    }
                                                    else
                                                    {
                                                        echo $this->requestAction('admin/users/change_datetimeformat/'.strtotime($orderExist[0]->shipped_date));
                                                    }
                                                } 
                                                else
                                                {
                                                    echo "Not shipped yet.";
                                                }
                                                ?>
                                                    
                                            </tr>
                                            <?php if($orderExist[0]->shipping_status=='R')
                                            { ?>
                                                <tr>
                                                    <td><b>Canceled Date:</b></td>
                                                    <td> <?php

                                                    if(!empty($orderExist[0]->cancelled_date) && $orderExist[0]->cancelled_date!='0000-00-00 00:00:00')
                                                    {
                                                        echo $this->requestAction('admin/users/change_datetimeformat/'.strtotime($orderExist[0]->cancelled_date));
                                                    } 
                                                    else 
                                                    {
                                                        echo "N/A";
                                                    }
                                                    ?> </td>
                                                </tr>
                                                <?php
                                            }
                                            else
                                            { ?>
                                            <tr>
                                                <td><b>Delivered Date:</b></td>
                                                <td> <?php
                                                
                                                if(!empty($orderExist[0]->reached_date) && $orderExist[0]->reached_date!='0000-00-00 00:00:00')
                                                {
                                                    echo $this->requestAction('admin/users/change_datetimeformat/'.strtotime($orderExist[0]->reached_date));
                                                } 
                                                else 
                                                {
                                                    echo "Not delivered yet.";
                                                }
                                                ?> </td>
                                            </tr>
                                            <?php
                                            } ?>
                                            
                                        </table>
                                    </div>
                                
                            </div>
                        </div>

                       <?php if (!empty($presc)) { ?>
                            <header class="main-content-header" style="padding-left: 10px">
                                <h2 class="my-orders"> Download Prescription </h2>
                            </header>
                            <div class="order-conteainer">
                                <div class="order-detail-content">
                                    
                                        <div class="col-md-12">
                                            <table class="table">
                                                <?php foreach ($presc as $pFile) { ?>
                                                    <tr>
                                                        <td><b>Click To Download :</b></td>
                                                        <td>

                                                            <a href="<?php echo $this->request->webroot . 'prescription' . DS . $pFile->file; ?>" target="_blank" download >Prescription</a>
                                                            <?php //echo  $this->response->file('prescription' . DS . $pFile->file, array('download' => true, 'name' => 'Download')); ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>

                                            </table>
                                        </div>
                                    
                                </div>
                            </div>   
                        <?php } ?>

                        <?php foreach ($orderExist as $appOrd) { ?> 
                            <div class="my-order-tab">
                                <header class="main-content-header" style="padding-left: 10px">
                                    <h2 class="my-orders">  <?php echo $appOrd['treatment']['name'] ?></h2>
                                </header>
                                <div class="order-conteainer">
                                    <div class="order-body-content">
                                        
                                            <div class="col-md-12 col-sm-12">
                                                <div class="order-list-content table-responsive">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th><b>Medicine</b></th>
                                                                        <th><b>Pill Name</b></th>
<!--                                                                        <th><b>Qty</b></th>-->
                                                                        <th><b>Price</b></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php foreach ($appOrd['orderdetails'] as $appDet) { ?>
                                                                        <tr>
                                                                            <th scope="row"> <?php echo $appDet['medicine']['title'] ?> </th>
                                                                            <td> <?php echo $appDet['pil_name'] ?> </td>
<!--                                                                            <td> <?php echo $appDet['pil_qty'] ?> </td>-->
                                                                            <td> £<?php echo sprintf('%0.2f', $appDet['pil_price']) ?> </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                    </div>
                                </div>
                            </div>    
                        <?php } ?>    
                        

                        
                        
                            <?php if ($orderExist[0]->verifiedby != "" && $orderExist[0]->verifiedbytype == "Doctor") { ?>
                            <div class="order-conteainer" id="rejectPrescd" style="display: block;margin-bottom:20px;" >
                                <div class="order-detail-content">
                                    
                                        <div class="col-md-12">        
                                            <?php echo $this->Form->create('', ['class' => 'form-horizontal', 'id' => 'smsgprecdet_validate']); ?>
                                            <input type="hidden" name="transid" value="<?php echo $orderExist[0]->transaction_id; ?>" >
                                            <input type="hidden" name="ftype" value="msg" >
                                            <input type="hidden" name="fromid" value="<?php echo $user->id ?>" >
                                            <input type="hidden" name="toid" value="<?php echo $orderExist[0]->verifiedby; ?>" >
                                            <input type="hidden" name="pid" value="<?php echo $orderExist[0]->user_id; ?>" >
                                            <input type="hidden" name="type" value="d" >
                                            <input type="hidden" name="fromtype" value="admin" > 
                                            <input type="hidden" name="totype" value="doctor" >


                                            <div class="form-group">
                                                <label for="email">Message To Doctor:</label>
                                                <textarea class="form-control" id="msg" name="msg" rows="4" cols="50"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-default">Send Message</button>
                                            <?php echo $this->Form->end(); ?>                                      
                                        </div>
                                    
                                </div>
                            </div>     
                            <?php } ?>

                        <div class="order-conteainer" id="rejectPrescp" style="display: block" >
                            <div class="order-detail-content">
                                
                                    <div class="col-md-12">        
                                        <?php echo $this->Form->create('', ['class' => 'form-horizontal', 'id' => 'smsgprecdet_validate']); ?>
                                        <input type="hidden" name="transid" value="<?php echo $orderExist[0]->transaction_id; ?>" >
                                        <input type="hidden" name="ftype" value="msg" >
                                        <input type="hidden" name="fromid" value="<?php echo $user->id ?>" >
                                        <input type="hidden" name="toid" value="<?php echo $orderExist[0]->user_id; ?>" >
                                        <input type="hidden" name="pid" value="<?php echo $orderExist[0]->user_id; ?>" >
                                        <input type="hidden" name="type" value="p" >
                                        <input type="hidden" name="fromtype" value="admin" > 
                                        <input type="hidden" name="totype" value="patient" >
                                        <div class="form-group">
                                            <label for="email">Message To Patient:</label>
                                            <textarea class="form-control" id="msg" name="msg" rows="4" cols="50"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default">Send Message</button>
                                        <?php echo $this->Form->end(); ?>                                      
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
<!--END PAGE CONTENT -->


<script>
    function rejectNow() {
        $('#rejectPresc').css('display', 'block');
    }
</script>