<!--PAGE CONTENT -->
<div id="content">
  <div class="inner">
    <div class="row">
      <div class="col-lg-12">
        <h2> <?php echo  __('Screenshots Management') ?> </h2>
      </div>
    </div>
    <hr />
    <div class="row">
      <div class="col-lg-12">
          <div class="box">
            
                    <section class="panel">
                        <header class="panel-heading">
                            <b>Screenshot Details</b>
                        </header>
                    <div class="panel-body">
                    <div class="form">
                        <?php //echo $this->Form->create($content) ?>
                        <?php echo $this->Form->create($homecontent,['enctype'=>'multipart/form-data','class' => 'cmxform form-horizontal adminex-form', 'id' => 'admin-validate']);?>
                        <div class="form-group ">
                            <label for="username" class="control-label col-lg-2">Title</label>
                            <div class="col-lg-10">
                               <?php echo $this->Form->input('screenshot_title', array('class'=>'form-control','label' => false)); ?> 
                            </div>
                        </div>
                        
                        <div class="form-group ">
                            <label for="username" class="control-label col-lg-2">Description</label>
                            <div class="col-lg-10">
                               <?php echo $this->Form->input('screenshot_description', array('class'=>'form-control','label' => false)); ?> 
                            </div>
                        </div>
                         
                       
                            <button type="submit" class="btn btn-primary" style="margin-top: 15px">Save</button>
                        

                        <?php //echo $this->Form->button(__('Submit')) ?>
                        <?php echo $this->Form->end() ?>
                    </div>
                    </div>
                    </section>
                
        </div>
          
        <div class="panel panel-default">
          <div class="panel-heading"> <?php echo  __('Screenshot Management') ?> 
              <a href="<?php echo $this->Url->build(["action" => "add"]); ?>"> <button class="btn btn-info btn-xs pull-right"><i class="icon-cogs icon-white"></i> Add New </button>  </a>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th style="width: 5%">Sl No</th>
                    <th ><?php echo $this->Paginator->sort('Image') ?></th>
                    <th ><?php echo $this->Paginator->sort('position') ?></th>
                    
                    <th ><?php echo  __('Actions') ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  <?php foreach ($goals as $dt): ?>  
                  <tr>
                    <td> <?php echo $i ?> </td>
                    
                    <td><?php
                        if(!empty($dt->image))
                        {
                            echo($this->Html->image('/easydownload/'.$dt->image, array('alt' => $dt->title, 'width' => '100' )));
                        } ?></td>
                    <td><?php echo h($dt->position) ?></td>
                    <td>
                      <a href="<?php echo $this->Url->build(["action" => "edit", $dt->id]); ?>"> <button class="btn btn-primary"><i class="icon-pencil icon-white"></i> Edit</button>  </a>
                      <?php echo $this->Form->postLink(
                        'Delete',
                        ['action' => 'delete', $dt->id],
                        ['confirm' => 'Are you sure, you want to delete this row?','class' => 'btn btn-danger']) ?>
                    </td>
                  </tr>
                  <?php $i ++; ?>
                  <?php endforeach; ?>
                </tbody>
              </table>
              <div class="paginator">
                  <ul class="pagination">
                      <?php echo  $this->Paginator->prev('< ' . __('previous')) ?>
                      <?php echo  $this->Paginator->numbers() ?>
                      <?php echo  $this->Paginator->next(__('next') . ' >') ?>
                  </ul>
                  <p><?php //echo  $this->Paginator->counter() ?></p>
              </div>                  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--END PAGE CONTENT --> 







<!--<div id="content">
    <div class="inner">
      <div class="row">
        <div class="col-lg-12">
         
        </div>
      </div>
      <hr />
<div class="table-responsive">
    <h3><?php echo  __('CMS PAGES LIST') ?></h3>
    <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id') ?></th>
                <th><?php echo $this->Paginator->sort('page_title') ?></th>
                <th><?php echo $this->Paginator->sort('page_slug') ?></th>
                <th class="actions"><?php echo  __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contents as $dt): ?>
            <tr>
                <td><?php echo $this->Number->format($dt->id) ?></td>
                <td><?php echo h($dt->page_title) ?></td>
                <td><?php echo h($dt->page_slug) ?></td>
                <td class="actions">
                    <?php //echo //$this->Html->link(__('View'), ['action' => 'view', $dt->id]) ?>
                    <?php //echo $this->Html->link(__('Edit'), ['action' => 'edit', $dt->id]) ?>
                    
                    <a href="<?php echo $this->Url->build(["action" => "edit", $dt->id]); ?>"> <button class="btn btn-primary"><i class="icon-pencil icon-white"></i> Edit</button>  </a>
                    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?php echo  $this->Paginator->prev('< ' . __('previous')) ?>
            <?php echo  $this->Paginator->numbers() ?>
            <?php echo  $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?php echo  $this->Paginator->counter() ?></p>
    </div>
</div>
</div>
</div>-->

