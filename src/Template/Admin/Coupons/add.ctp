<?php ?>
<div id="content">
    <div class="inner">
        <div class="row">
            <div class="col-lg-12">
                <h1 > Add Discount Code </h1>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <header>
                        <div class="icons"><i class="icon-th-large"></i></div>
                        <h5>Add Discount Code</h5>
                        <div class="toolbar">
                            <ul class="nav">
                                <li style="margin-right:15px">
                                    <div class="btn-group"> 

                                    </div>
                                </li>

                            </ul>
                        </div>
                    </header>
                    <div id="collapseOne" class="accordion-body collapse in body">
                        <div class="col-sm-6">

                            <div class="row">
				  <?php echo $this->Form->create($crown,['class' => 'form-horizontal', 'id' => 'user-validate']);?>

                                <input type="hidden" name="id" id="id" value="<?php echo (!empty($crown->id)?$crown->id:''); ?>" />

                                <div class="form-group">
                                    <label class="control-label col-lg-4">Code Title</label>
                                    <div class="col-lg-8">
                                        <input type="text" id="title" name="title" class="form-control" value="<?php echo (!empty($crown->title)?$crown->title:''); ?>" required=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Discount Price</label>
                                    <div class="col-lg-8">
                                        <input type="number" id="price" name="price" class="form-control" value="<?php echo (!empty($crown->price)?$crown->price:''); ?>" required=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Discount User</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" required name="coupon_for">
                                            <option value="">Select</option>
                                            <option value="D" <?php echo (!empty($crown->coupon_for) && $crown->coupon_for=='D'?'selected':''); ?>>Doctor</option>
                                            <option value="T" <?php echo (!empty($crown->coupon_for) && $crown->coupon_for=='T'?'selected':''); ?>>Technician</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Discount Code</label>
                                    <div class="col-lg-8">
                                        <input type="text" id="code" name="code" class="form-control" value="<?php echo (!empty($crown->code)?$crown->code:''); ?>" required=""/>
                                        <input type="button" value="Generate" class="btn btn-primary" onclick="generate_code()">
                                    </div>
                                </div>
                                
                                                      

                                <div class="form-group">
                                    <label class="control-label col-lg-4">Active</label>
                                    <div class="col-lg-8">
                                        <input type="checkbox" id="is_active" name="is_active" <?php echo (!empty($crown->is_active)?'checked':''); ?> style="width:auto" class="form-control" value="1" />
                                    </div>
                                </div>

                                
                                <label class="control-label col-lg-4"></label>
                                <div class="col-lg-8" style="text-align:left;"> 
                                    <input type="submit" name="submit" value="Save" class="btn btn-primary" />
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  for (var i = 0; i < 8; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
function generate_code()
{
    document.getElementById('code').value = makeid();
}
</script>




