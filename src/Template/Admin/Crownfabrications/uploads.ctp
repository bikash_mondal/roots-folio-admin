
<?php echo $this->Html->script('/plugins/validationengine/js/jquery.validationEngine.js')?>
<?php echo $this->Html->script('/plugins/validationengine/js/languages/jquery.validationEngine-en.js')?>
<?php echo $this->Html->script('/plugins/jquery-validation-1.11.1/dist/jquery.validate.min.js')?>
<?php echo $this->Html->script('admin/validationInit.js')?> 
<?php echo $this->Html->script('/plugins/jasny/js/bootstrap-fileupload.js')?>

<script type="text/javascript">
	$(function () { formValidation(); });
</script>


<div id="content">
    <div class="inner">
      <div class="row">
        <div class="col-lg-12">
          <h1>Upload Images</h1>
        </div>
      </div>
      <hr/>
	  <?php echo  $this->Flash->render('success') ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <header>
              <div class="icons"><i class="icon-th-large"></i></div>
              <h5>Upload Images</h5>
            </header>
            <div id="collapseOne" class="accordion-body collapse in body">
              <div class="col-sm-6">
                  
                  <div class="row">
			<?php echo  $this->Form->create($crown, ['class' => 'form-horizontal', 'id'=>'block-validate','enctype'=>"multipart/form-data"]) ?>
                      <input type="hidden"  name="crown_id" value="<?php echo $this->request->data["id"]; ?>">
                        
                        <div class="form-group">
                          <label class="control-label col-lg-4" >Photos</label>
                          <div class="col-lg-8">
                          <?php
                          foreach($this->request->data["Crownfabricimages"] as $img)
                          {
                          ?>
                            <div class="col-md-6" id="imgdiv<?php echo $img["id"]; ?>">
                                <img src="<?php echo $this->Url->build('/crownimg/'.$img["image"]); ?>" style=" height:150px; width:150px;" >
                                <a href="javascript:void(0)" onclick="deleteimg(<?php echo $img["id"] ?>);"><img src="<?php echo $this->Url->build('/cross.png'); ?>"></a>
                            </div>
                                    <?php }?>    
                             
                          </div>                       
                        </div> 
                        <div class="form-group">
                          <label class="control-label col-lg-4" >Image</label>
                          <div class="col-lg-8">
                              <input type="file"  name="image" required="">
                          </div>                       
                        </div> 
                        <div class="form-actions no-margin-bottom" style="text-align:center;">
                          <input type="submit" name="submit" value="Upload" class="btn btn-primary" />
                        </div>
                      <?php echo $this->Form->end();?>
                  </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script>
    function deleteimg(id)
    {
        var tt=confirm("Are you want to remove this?");
        if(tt)
        {
            $.get("<?php echo $this->request->webroot?>admin/crownfabrications/delete_photo/"+id,function(data){
            if(data.trim()==1)
            {
                $("#imgdiv"+id).remove();
            }
                
                
            });
        }
        
        
    }
</script>    