<?php // echo $this->Html->script('ckeditor/ckeditor.js') ?>


  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
  


<div id="content">
<div class="inner">
<div class="row">
<div class="col-lg-12">

</div>
</div>
<hr />
<div class="wrapper">
<div class="row">
    <div class="col-lg-12">
          <div class="box">
            
                    <section class="panel">
                        <header class="panel-heading">
                            <b>Why Choose Management</b>
                        </header>
                    <div class="panel-body">
                    <div class="form">
                        <?php //echo $this->Form->create($content) ?>
                        <?php echo $this->Form->create($run,['enctype'=>'multipart/form-data','class' => 'cmxform form-horizontal adminex-form', 'id' => 'admin-validate']);?>
                        <div class="form-group ">
                            <label for="username" class="control-label col-lg-2">Title</label>
                            <div class="col-lg-10">
                               <?php echo $this->Form->input('title', array('class'=>'form-control','label' => false)); ?> 
                            </div>
                        </div>
                         <div class="form-group ">
                            <label for="username" class="control-label col-lg-2">Text</label>
                            <div class="col-lg-10">
                            <?php
                                
                                echo $this->Form->input('text', array('class'=>'form-control','label' => false));
                            ?>
                            </div>
                         </div>
                        
                         <?php 
                            if(!empty($content->image)) { ?>
                                  <div class="form-group " style="text-align:center;">
                                        <div class="col-lg-10">
										<?php
										
                                        $uploadFolder = "easydownload";
                                        $uploadPath = WWW_ROOT . $uploadFolder;
                                        $invoiceImageName = $content->image;
                                        if(file_exists($uploadPath . '/' . $invoiceImageName) && $invoiceImageName!=''){
                                            echo($this->Html->image('/easydownload/'.$invoiceImageName, array('alt' => $content->title, 'width' => '100' )));
                                        } else {
                                            echo($this->Html->image('/easydownload/default.png', array('alt' => $content->title,  'width' => '100')));
                                        }
                                  
                                      ?>          
                                        </div>
                                  </div>
                                        
                           <?php } ?>
                                    <div class="form-group ">
                                        <label for="username" class="control-label col-lg-2">Image</label>
                                        <div class="col-lg-10">

					    <?php echo $this->Form->input('image',array('class'=>'form-control ','type'=>'file','label'=>false));?>
                                           
                                        </div>
                                    </div>
                       
                            <button type="submit" class="btn btn-primary" style="margin-top: 15px">Save</button>
                        

                        <?php //echo $this->Form->button(__('Submit')) ?>
                        <?php echo $this->Form->end() ?>
                    </div>
                    </div>
                    </section>
                
        </div>
    </div>
</div>
</div>
</div>
</div>
