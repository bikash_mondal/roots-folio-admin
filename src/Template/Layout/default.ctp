<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'Perfect Shade';
?>


<!DOCTYPE HTML>
<html lang="en">
    <head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-KRSPKQM');</script>
	<!-- End Google Tag Manager -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php $filePathf = WWW_ROOT . 'logo' . DS . $SiteSettings['site_favicon']; ?>
        <?php if ($SiteSettings['site_favicon'] != "" && file_exists($filePathf)) { ?>
            <?php echo $this->Html->meta('favicon.ico', 'logo/' . $SiteSettings['site_favicon'], array('type' => 'icon')); ?>
        <?php } else { ?>
            <?php echo $this->Html->meta('favicon.ico', 'img/unnamed.png', array('type' => 'icon')); ?>
        <?php } ?>


        <?php if(empty($pageSeo)){ ?>
            <title> <?php echo $SiteSettings['site_meta_title'] ?> </title>
            <meta name="description" content="<?php echo $SiteSettings['site_meta_description'] ?>" />
<!--            <meta name="keywords" content="<?php echo $SiteSettings['site_meta_key'] ?>" />            -->
        <?php } else if(!empty($pageSeo)){ ?>
            <title> <?php echo $pageSeo['site_meta_title'] != "" ? $pageSeo['site_meta_title'] : 'Ascot Pharmacy' ?> </title>
            <meta name="description" content="<?php echo $pageSeo['site_meta_description'] ?>" />
<!--            <meta name="keywords" content="<?php echo $pageSeo['site_meta_key'] ?>" />        -->
        <?php } else { ?>
            <title> Ascot Pharmacy </title>
            <meta name="description" content="" />
            <meta name="keywords" content="" />
        <?php } ?>
        <?php #echo $this->Html->js('jquery.min.js') ?>
        <?php echo $this->Html->script('jquery.min');?>
        <?php echo $this->Html->script('bootstrap.min');?>
<!--            <script src="js/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>-->




        <?php echo $this->Html->css('bootstrap.css') ?>
        <?php echo $this->Html->css('bootstrap-theme.css') ?>
        <?php echo $this->Html->css('/font-awesome/css/font-awesome.css') ?>
        <?php echo $this->Html->css('/css/input-style.css') ?>
        <?php echo $this->Html->css('/css/ionic.css') ?>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.css" rel="stylesheet">





        <?php #echo $SiteSettings['site_meta_tags'];?>

        <style>
            .message.success{
                background: #5cb85c none repeat scroll 0 0;
                color: #fff;
                font-weight: bold;
                padding: 12px 10px;
                text-align: center;
                font-size : 18px;
                position: absolute;
                width: 100%;
                z-index: 555;
                margin-top: 0px
            }
            .message.error{
                background: #fa693c none repeat scroll 0 0;
                color: #fff;
                font-weight: bold;
                padding: 12px 10px;
                text-align: center;
                font-size : 18px;
            }
        </style>



    </head>
    <body>




        <?php #if ($this->request->params['controller'] == "Pages" && $this->request->params['action'] == "home") { ?>

             <?php echo $this->element('home_header'); ?>
                   <?php echo $this->Flash->render() ?>
        <?php echo $this->Flash->render('success') ?>
        <?php echo $this->Flash->render('error') ?>
        <?php /*}
        else { ?>
            <?php #echo $this->element('header'); ?>
        <?php } */ ?>
        <?php echo $this->fetch('content'); ?>
        <?php echo $this->element('footer'); ?>

    </body>


    <script>
       setTimeout(function(){
           $('.message.success').hide()
       },5000);
    </script>

</html>

<?php #echo $this->element('sql_dump');  ?>
