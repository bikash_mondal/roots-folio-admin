<div class="treatments-prices-banner">
    <img src="<?php echo $this->Url->build('/', true);?>images/treatment-banner.jpg" alt=""/> 
    <div class="treatments-banner-content">
        <h1>Erectile dysfunction</h1>
        <p>Erectile dysfunction (also known as impotence) is a very common condition, which affects the majority of men at some point in their lives. Up to 50% of men aged 40 - 70 and up to 70% of men over the age of 70 suffer from erectile dysfunction to some extent. </p>
        <button type="button" class="btn btn-primary">Read More</button>
    </div>
</div>
<div class="treatments-body-aarea">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#treatment-prices" aria-controls="treatment-prices" role="tab" data-toggle="tab">Treatment And Prices</a></li>
                    <li role="presentation"><a href="#medical-information" aria-controls="medical-information" role="tab" data-toggle="tab">Medical Information </a></li>
                    <li role="presentation"><a href="#buy-treatment" aria-controls="buy-treatment" role="tab" data-toggle="tab">Buy-Treatment</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="treatment-prices">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="viagra-product">
                                    <div class="viagra-product-pic"><img src="<?php echo $this->Url->build('/', true);?>images/viagra-pic.jpg" /></div>
                                    <h3>Viagra</h3>
                                    <div class="viagra-product-text">
                                        <p>Viagra (sildenafil)<br>
                                            25mg, 50mg, 100mg from £5.13 per <br>
                                            tablet</p>
                                        `<button type="button" class="btn btn-success">Buy treatment</button>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-md-3">
                                <div class="viagra-product">
                                    <div class="viagra-product-pic"><img src="<?php echo $this->Url->build('/', true);?>images/generic-sildenafil-pic.jpg" /></div>
                                    <h3>Generic Sildenafil</h3>
                                    <div class="viagra-product-text">
                                        <p>Sildenafil (generic Viagra)<br>
                                            25mg, 50mg, 100mg from £1.50 per <br>
                                            tablet</p>
                                        `<button type="button" class="btn btn-success">Buy treatment</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="viagra-product">
                                    <div class="viagra-product-pic"><img src="<?php echo $this->Url->build('/', true);?>images/cialis.jpg" /></div>
                                    <h3>Cialis</h3>
                                    <div class="viagra-product-text">
                                        <p>Cialis (tadalafil)<br>
                                            2.5mg, 5mg, 10mg, 20mg from £2.47 per
                                            tablet</p>
                                        `<button type="button" class="btn btn-success">Buy treatment</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="viagra-product">
                                    <div class="viagra-product-pic"><img src="<?php echo $this->Url->build('/', true);?>images/spedra.jpg" /></div>
                                    <h3>Viagra</h3>
                                    <div class="viagra-product-text">
                                        <p>Viagra (sildenafil)<br>
                                            25mg, 50mg, 100mg from £5.13 per <br>
                                            tablet</p>
                                        `<button type="button" class="btn btn-success">Buy treatment</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="viagra-product">
                                    <div class="viagra-product-pic"><img src="<?php echo $this->Url->build('/', true);?>images/spedra-b.jpg" /></div>
                                    <h3>Viagra</h3>
                                    <div class="viagra-product-text">
                                        <p>Viagra (sildenafil)<br>
                                            25mg, 50mg, 100mg from £5.13 per <br>
                                            tablet</p>
                                        `<button type="button" class="btn btn-success">Buy treatment</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="viagra-product">
                                    <div class="viagra-product-pic"><img src="<?php echo $this->Url->build('/', true);?>images/levitra.jpg" /></div>
                                    <h3>Viagra</h3>
                                    <div class="viagra-product-text">
                                        <p>Viagra (sildenafil)<br>
                                            25mg, 50mg, 100mg from £5.13 per <br>
                                            tablet</p>
                                        `<button type="button" class="btn btn-success">Buy treatment</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="viagra-product">
                                    <div class="viagra-product-pic"><img src="<?php echo $this->Url->build('/', true);?>images/spedra-c.jpg" /></div>
                                    <h3>Viagra</h3>
                                    <div class="viagra-product-text">
                                        <p>Viagra (sildenafil)<br>
                                            25mg, 50mg, 100mg from £5.13 per <br>
                                            tablet</p>
                                        `<button type="button" class="btn btn-success">Buy treatment</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="viagra-product">
                                    <div class="viagra-product-pic"><img src="<?php echo $this->Url->build('/', true);?>images/viagra-pic.jpg" /></div>
                                    <h3>Viagra</h3>
                                    <div class="viagra-product-text">
                                        <p>Viagra (sildenafil)<br>
                                            25mg, 50mg, 100mg from £5.13 per <br>
                                            tablet</p>
                                        `<button type="button" class="btn btn-success">Buy treatment</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="medical-information">...</div>
                    <div role="tabpanel" class="tab-pane" id="buy-treatment">
                        <h3>Answer medical questions to order</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>