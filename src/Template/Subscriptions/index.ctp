<?php ?>
<!DOCTYPE HTML>

	<section class="details-top">
		<div class="container">
			<ul class="bredcumb">
				<li><a href="<?php echo $this->Url->build('/'); ?>">Home</a></li>
				<li class="active"><a href="">Subscriptions or Service name</a></li>
			</ul>
		</div>
	</section>
	
	<section class="details-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="cards">
						<div class="card-box">
							<div class="row">
								<div class="col-md-5 col-sm-6">
									<div class="product-img-box">
                                                                            <img src="<?php echo $this->request->webroot; ?>img/imgpsh_fullsize.png" alt="">
									</div>
								</div>
								<div class="col-md-7 col-sm-6">
									<div class="right-prod-info">
										<h2><?php echo $subscriptions->title ?></h2>
										<?php
                                                                                echo $subscriptions->description;
                                                                                ?>
										<h3 class="price margin-bot-20">Price: <span>$<?php echo number_format($subscriptions->price, 2);?></span></h3>
<!--                                                                                <p><a href="<?php echo $this->Url->build('/'); ?>subscriptions/payment_new/<?php echo !empty($user_id)?$user_id:''; ?>" class="btn btn-primary btn-lg">Order Now</a></p>-->
                                                                                <p><a href="<?php echo $this->Url->build(["controller" => "Products", "action" => "add",'subscription']); ?>" class="btn btn-primary btn-lg">Add to Cart</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	


