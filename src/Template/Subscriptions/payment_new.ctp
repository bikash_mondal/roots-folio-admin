<?php ?>
<section class="details-top">
   <div class="container">
      <ul class="bredcumb">
         <li><a href="<?php echo $this->Url->build('/'); ?>">Home</a></li>
         <li class="active"><a href="">Payment</a></li>
      </ul>
   </div>
</section>
<section class="details-wrapper">
   <div class="container">
      <div class="row">
         <div class="col-md-9 col-sm-8">
            <div class="cards">
               <div class="card-box">
                  <div class="row checkout-holder">
                      <form method="post" id="shippingform" onsubmit="return valid()">
                          <input type="hidden" name="cc_type" id="cc_type">   
                        <div class="col-md-12">
                           <h3>Your Cart / Payment Confirmation</h3>
                           <hr>
                           <h4>Your Information</h4>
                           <div>
                              <div class="form-group">
                                  <input type="text" placeholder="First Name" id="exampleInputEmail3" class="form-control" required="" 
                                         name="first_name" value="<?php echo !empty($user->first_name)?$user->first_name:"" ?>">
                              </div>
                              <div class="form-group">
                                  <input type="text" placeholder="Last Name" id="exampleInputPassword3" class="form-control" required=""      
                                  name="last_name" value="<?php echo !empty($user->last_name)?$user->last_name:'' ?>">
                              </div>
                             <div class="form-group">
                                  <input type="text" placeholder="Email" id="exampleInputPassword3" class="form-control" required="" name="email"
                                         value="<?php echo !empty($user->email)?$user->email:''; ?>"
                                         >
                              </div>
                             <div class="form-group">
                                  <input type="text" placeholder="Phone" id="exampleInputPassword3" class="form-control" required="" name="phone">
                              </div>  
                               
                           </div>
                           <hr>
                        </div>
                         <div class="col-md-12">
                            <h4>Put Your Card Information</h4>
                           <div class="form-group">
                               <input type="text" placeholder="First Name on Card" class="form-control" required="" name="cc_firstname" >
                           </div>
                           <div class="form-group">
                               <input type="text" placeholder="Last Name on Card" class="form-control" name="cc_lastname" id="cc_lastname" required="">
                           </div>
                            <div class="form-group">
                               <input type="text"  placeholder="Card Number" class="txtboxToFilter form-control" name="cc_no" required="" maxlength="16" minlength="14"  id="cc_no">

                            </div>
                            <div class="form-group row">
                                
                                <div class="col-md-6">   
                                <input type="number" min="1" max="12"  placeholder="Expiration Month" class="txtboxToFilter form-control" required="" name="exp_month" id="exp_month">
                                </div>
                                <div class="col-md-6">   
                                <input type="number" min="<?php echo date('Y'); ?>" max="<?php echo date('Y')+25; ?>"  placeholder="Expiration Year" class="txtboxToFilter form-control" required="" name="exp_year" id="exp_year">
                                </div>
                                       
                                
                               
                            </div>
                            <div class="clearfix"></div> 
                           <div class="form-group">
                               <input type="password"  placeholder="CVV" class="txtboxToFilter form-control" required="" name="cvv" id="cvv">

                           </div>
                           
                        </div>    
                        <div class="col-md-6">
                           <h4>Billing Address</h4>
                           <div class="form-group">
                               <input type="text" placeholder="Address 1" class="form-control" required="" name="billing_address1" id="billing_address1">
                           </div>
                           <div class="form-group">
                               <input type="text" placeholder="Address 2" class="form-control" name="billing_address2" id="billing_address2" >
                           </div>
                           <div class="form-group">
                               <input type="text" placeholder="Country" class="form-control" required="" id="billing_country" name="billing_country">

                           </div>
                           <div class="form-group">
                               <input type="text" placeholder="State" class="form-control" required="" id="billing_state" name="billing_state">

                           </div>
                           
                           <div class="form-group">
                               <input type="text" placeholder="City" class="form-control" required="" id="billing_city" name="billing_city">

                           </div>
                           <div class="form-group">
                               <input type="text" placeholder="Zip Code" class="form-control" name="billing_zip" id="billing_zip">
                           </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Shipping Address <span class="same"><input type="checkbox" value="1" id="same"> Same as Billing</span></h4>
                           <div class="form-group">
                               <input type="text" placeholder="Address 1" class="shipping form-control" required="" name="shipping_address1" id="shipping_address1">
                           </div>
                           <div class="form-group">
                               <input type="text" placeholder="Address 2" class="shipping form-control" name="shipping_address2" id="shipping_address2">
                           </div>
                            <div class="form-group">
                               <input type="text"  placeholder="Country" class="shipping form-control" required="" name="shipping_country" id="shipping_country">

                            </div>
                            <div class="form-group">
                               <input type="text"  placeholder="State" class="shipping form-control" required="" name="shipping_state" id="shipping_state">

                            </div>
                           <div class="form-group">
                               <input type="text"  placeholder="City" class="shipping form-control" required="" name="shipping_city" id="shipping_city">

                           </div>
                           <div class="form-group">
                               <input type="text" placeholder="Zip Code" class="shipping form-control" required="" name="shipping_zip" id="shipping_zip">
                           </div>
                        </div>
                        
                        <div class="col-md-12">
                           <hr>
                          <h4>Discount Code</h4>
                           <div class="form-inline">
                              <div class="form-group">
                                  <input type="text" placeholder="Enter Coupon Code" id="discount_code" name="discount_code" class="form-control">
<!--                                 <input type="hidden" name="coupon_id" id="coupo">-->
                                 <div class="clearfix"></div>
                                 <span style="color:green;font-weight: bold;display: none" class="success_message coupon_message"></span>
                                 <span style="color:red;font-weight: bold;display: none" class="error_message coupon_message"></span>
                              </div>
                              
                               <div class="form-group" style="vertical-align: top">
                                  <input type="button" id="exampleInputPassword3" class="btn btn-primary" value="Apply" onclick="validateCode()">
                              </div>
                           </div>
                           <hr>
                           <p class="text-right"><input type="submit" value="Submit Payment" class="btn btn-primary btn-lg"></p>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-3 col-sm-4">
            <div class="cards">
               <div class="card-box">
                  <h4>Payment Summary</h4>
<!--                  <p><button type="button" class="btn btn-block btn-lg card-btn"><i class="fa fa-credit-card"></i> Card</button></p>
                      <p><button type="button" class="btn btn-block btn-lg paypal-btn"><i class="fa fa-paypal"></i> Paypal</button></p>-->
                  <div class=" table-responsive">
                      <table class="table">
                          <tr>
                              <td>Product:</td>
                              <td><?php echo $subscriptions->title; ?></td>
                          </tr>
                          
                          
                           <tr>
                              <td>Quantity:</td>
                              <td>1</td>
                          </tr>
                          <?php
                          if(!empty($user_id))
                          {
                            if($user->utype == 2){
                          $net_price= $subscription->price+ $product->price; 
                          }
                          else{
                            $net_price=$subscription;
                          } 
                          ?>
                          <tr>
                              <td>Subscriptions Charge:</td>
                              <td>$<?php echo !empty($subscription->price)?number_format($subscription->price, 2, ',', ' '):0;?></td>
                          </tr>
                          <?php }else{?>
                          <?php $net_price=$subscriptions->price;  ?>

                          <?php }?>
                          <tr>
                              <td>Total:</td>
                              
                              <td>$<?php echo number_format(($net_price), 2, ',', ' ');?></td>
                          </tr>
                          
                          
                      </table>
                  </div>
                  
                  <h4>Policies</h4>
                  <p><input type="checkbox" id="shopping_policy"> Shopping Policy</p>
                  <p><input type="checkbox" id="legal_policy"> Legal Policy</p>
                  <hr>
                  <h4>Secure Payment</h4>
                  <p><img class="img-responsive" alt="" src="<?php echo $this->Url->build('/'); ?>images/paypal.png"></p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<?php echo $this->Html->script('classie', array('inline' => false));?>
<script>
    function validateCode()
    {
        if($('#discount_code').val().trim())
        {
            $.ajax({
                type:'POST',
                url:'<?php echo $this->Url->build(array('controller' => 'Products', 'action' => 'redeem')); ?>',
                data:{code:$('#discount_code').val()},
                dataType:"json",
                success:function(data){
                    console.log(data)
                    $('.coupon_message').hide();
                    if(data.ack == 1)
                    {
                        $('.success_message').text(data.message);
                        $('.success_message').show();
                    }
                    else
                    {
                        $('.error_message').text(data.message);
                        $('.error_message').show();
                    }
                }
            })
        }
        else
        {
            alert("Please enter discount code.")
        }
    }
  $(document).ready(function(){
  $("#same").click(function(){
  if($(this).is(":checked"))    
  {
      $("#shipping_address1").attr("value",$("#billing_address1").val());
      $("#shipping_address2").attr("value",$("#billing_address2").val());
      $("#shipping_city").attr("value",$("#billing_city").val());
      $("#shipping_zip").attr("value",$("#billing_zip").val());
      $("#shipping_country").attr("value",$("#billing_country").val());
      $("#shipping_state").attr("value",$("#billing_state").val());
      $(".shipping").attr("readonly",true);
      

  }
  else
  {
            $(".shipping").attr("value","");
            $(".shipping").attr("readonly",false);
            

  }
  
  })  
  $(".txtboxToFilter").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });  
  $("#cc_no").blur(function(){
   var CardType=GetCardType($(this).val());
   $("#cc_type").attr("value",CardType);
   })

  
  })  
   
   function valid()
   {
       var shopping_policy=$("#shopping_policy").is(":checked");
       var legal_policy=$("#legal_policy").is(":checked");
       if(!shopping_policy || !legal_policy)
       {
           $("#errormodal").modal("show");
           return false;

       }
       
       
       
   }
   
   function init() {
       window.addEventListener('scroll', function(e){
           var distanceY = window.pageYOffset || document.documentElement.scrollTop,
               shrinkOn = 300,
               header = document.querySelector("header");
           if (distanceY > shrinkOn) {
               classie.add(header,"smaller");
           } else {
               if (classie.has(header,"smaller")) {
                   classie.remove(header,"smaller");
               }
           }
       });
   }
   
   function GetCardType(number)
{
    // visa
    var re = new RegExp("^4");
    if (number.match(re) != null)
        return "visa";

    // Mastercard
    re = new RegExp("^5[1-5]");
    if (number.match(re) != null)
        return "mastercard";

    // AMEX
    re = new RegExp("^3[47]");
    if (number.match(re) != null)
        return "amex";

    // Discover
    re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
        return "discover";

    

    

    // JCB
    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
        return "jcb";

   

    return "";
}
   window.onload = init();
</script>  
  <div class="modal fade" id="errormodal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Error</h4>
        </div>
        <div class="modal-body">
          <p>
              <strong>Please accept shopping and Legal Policy</strong>.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  

