

	<footer>
		<section class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="text-center">
              <a href="<?php echo $SiteSettings['ios_link']; ?>" target="_blank"><img src="<?php echo $this->Url->build('/'); ?>images/ios.png" alt=""></a>
							<a href="<?php echo $SiteSettings['androaid_link']; ?>" target="_blank"><img src="<?php echo $this->Url->build('/'); ?>images/android.png" alt=""></a>
						</p>
					</div>
					<div class="col-lg-12">
						<ul class="social-icons">
                                                        <li><a href="<?php echo $SiteSettings['facebook_url']; ?>" class="fa fa-facebook"></a></li>
                                                        <li><a href="<?php echo $SiteSettings['twitter_url']; ?>" class="fa fa-twitter"></a></li>
                                                        <li><a href="<?php echo $SiteSettings['gplus_url']; ?>" class="fa fa-google-plus"></a></li>
<!--							<li><a href=""><i class="fa ion-social-facebook"></i></a></li>
							<li><a href=""><i class="fa ion-social-twitter"></i></a></li>
							<li><a href=""><i class="fa ion-social-pinterest"></i></a></li>
							<li><a href=""><i class="fa ion-email"></i></a></li>-->
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="footer-bottom">
			<div class="container">
				<div class="row text-center">
					<div class="col-md-12">
						<ul class="footer-links">
                                                        <li><a href="<?php echo $this->Url->build('/'); ?>">Home</a></li>
                                                        <li><a href="<?php echo $this->Url->build('/how-it-works'); ?>">How it works</a></li>
                                                        <li><a href="<?php echo $this->Url->build('/about'); ?>">About</a></li>
                                                        <li><a href="<?php echo $this->Url->build('/faq'); ?>">Faq</a></li>
						</ul>
						<p class="copyright">© <?php echo date('Y');?> <?php echo $SiteSettings['footer_text1']; ?> </p>
					</div>
				</div>
			</div>
		</section>
	</footer>
