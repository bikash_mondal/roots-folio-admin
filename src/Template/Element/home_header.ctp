<?php
if ($this->request->params['controller'] == "Pages" && $this->request->params['action'] == "home") { ?>
<section class="homepage-top img-responsive" style="background: url('<?php echo $this->request->webroot; ?>images/<?= $SiteSettings['background_img']?>') no-repeat top center;">
<header class="navbar-fixed-top">
<?php }
else
{ ?>
<section>
<header class="navbar-fixed-top inner-header">
<?php } ?>

		<nav class="navbar navbar-default">
                <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                                <?php $filePathlo = WWW_ROOT . 'logo' . DS . $SiteSettings['site_logo']; ?>
                                <?php if ($SiteSettings['site_logo'] != "" && file_exists($filePathlo)) { ?>
                                    <a class="navbar-brand" href="<?php echo $this->Url->build('/'); ?>"> <img src="<?php echo $this->Url->build('/logo/' . $SiteSettings['site_logo']); ?>" /></a>
                                <?php } else { ?>
                                    <a class="navbar-brand" href="javascript:void(0)"></a>
                                <?php } ?>

                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                        <li><a href="<?php echo $this->Url->build('/'); ?>">Home</a></li>
                                        <li><a href="<?php echo $this->Url->build('/how-it-works'); ?>">How it works</a></li>
                                        <li><a href="<?php echo $this->Url->build('/about'); ?>">About</a></li>
                                        <li><a href="<?php echo $this->Url->build('/products'); ?>">Products</a></li>
<!--                                        <li><a href="<?php echo $this->Url->build('/subscriptions'); ?>">Subscriptions</a></li>-->
<!--                                        <li><a href="">Download</a></li>-->
                                        <li><a href="<?php echo $this->Url->build('/faq'); ?>">Faq</a></li>
                                        <li style="position:relative;top: -10px">
                                            <a href="<?php echo $this->Url->build('/products/payment-new'); ?>" style="border:none">
                                                <i class="fa fa-cart-plus" style="color: #fff;font-size: 35px;"></i>
                                                <?php if(!empty($total_cart_quantity))
                                                { ?>
                                                <span style="position: absolute;top: 9px;border-radius: 10px;background: #fff;color: #000;display: block;padding: 0px 4px;right: 0;"><?php echo $total_cart_quantity; ?></span>
                                                <?php } ?>
                                            </a>
                                        </li>
                                </ul>
                        </div>
                </div>
        </nav>
	</header>
        <?php
if ($this->request->params['controller'] == "Pages" && $this->request->params['action'] == "home") { ?>
        <div class="container">
                <div class="row">
                        <div class="col-xs-6">
                                <div class="banner-text-holder">
                                        <h1><?= $SiteSettings['header_text']; ?></h1>
                                        <p class="banner-btn">
                                                <a href="<?php echo $SiteSettings['ios_link']; ?>" target="_blank" class="class="app""  =><img src="<?php echo $this->request->webroot; ?>images/<?= $SiteSettings['banner_btn1_img']; ?>" alt=""></a>
                                                <a href="<?php echo $SiteSettings['androaid_link']; ?>" target="_blank" class="class="app""><img src="<?php echo $this->request->webroot; ?>images/<?= $SiteSettings['banner_btn2_img']; ?>" alt=""></a>
                                        </p>
                                </div>
                        </div>
                </div>
        </div>
<?php } ?>
</section>


<!-- js -->
<script src="<?php echo $this->request->webroot; ?>js/classie.js"></script>
<script>
    function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 300,
                header = document.querySelector("header");
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
            } else {
                if (classie.has(header,"smaller")) {
                    classie.remove(header,"smaller");
                }
            }
        });
    }
    window.onload = init();
</script>
