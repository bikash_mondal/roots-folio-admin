<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;
use Cake\Core\Configure;


class CartsTable extends Table
{

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
public function initialize(array $config)
{
    parent::initialize($config);

    //$this->setTable('carts');
}

/*
 * add a product to cart
 */
public function addProduct($productId) {
    $allProducts = $this->readProduct();

    if (!empty($allProducts)) {
        if (array_key_exists($productId, $allProducts)) {
             $allProducts[$productId] = 1;
        }
				else {
					$allProducts[$productId] = 1;
					$this->saveProduct($allProducts);

				}
    } else {
			//pr($allProducts);
        $allProducts[$productId] = 1;
        $this->saveProduct($allProducts);
    }
		//exit;
}

/*
 * get total count of products
 */
public function getCount() {
    $allProducts = $this->readProduct();

    if (count($allProducts)<1) {
        return 0;
    }

    $count = 0;
    foreach ($allProducts as $product) {
        $count=$count+$product;
    }

    return $count;
}

/*
 * save data to session
 */
public function saveProduct($data) {
    $session = new Session();
    return $session->write('cart',$data);
}

    /*
     * read cart data from session
     */
    public function readProduct() {
        $session = new Session();
        return $session->read('cart');
    }

    public function emptyCart()
    {
        $session = new Session();
        if($session->delete('cart'))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function deleteProduct($product)
    {
        $session = new Session();
        $cart = $session->read('cart');
        unset($cart[$product]);
        return $session->write('cart',$cart);
    }
}
