<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

//use Cake\I18n\FrozenDate;
use Cake\Database\Type; 
//Type::build('date')->setLocaleFormat('yyyy-MM-dd');

/**
 * Runs Controller
 *
 * @property \App\Model\Table\RunsTable $Runs
 */
class GoalsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event) {
        if (!$this->request->session()->check('Auth.Admin')) {
            return $this->redirect(
                            ['controller' => 'Users', 'action' => 'index']
            );
        }
    }

    public function index() {
        //pr($this->request->session()->check('Auth.Admin')); pr($this->request->session()->read('Auth.Admin')); exit;
        $this->loadModel('Homecontents');
        $homecontent = $this->Homecontents->get(1);
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            //pr($this->request->data); exit;
            $flag = true;
            if($this->request->data['goal_title'] == ""){
                $this->Flash->error(__('Title can not be null. Please, try again.')); $flag = false;
            }
            
            if($flag){
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png');
                if (!empty($this->request->data['goal_image']['name'])) {
                    $file = $this->request->data['goal_image']; //put the data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $fileName = time() . "." . $ext;
                    if (in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'easydownload' . DS . $fileName);
                        $this->request->data['goal_image'] = $fileName;
                    } else {
                        $flag = false;
                        $this->Flash->error(__('Upload image only jpg,jpeg,png files.'));
                    }
                } else {
                    unset($this->request->data['goal_image']);
                }                
            }                
            
            if($flag){             
               
                $content = $this->Homecontents->patchEntity($homecontent, $this->request->data);
                if ($this->Homecontents->save($content)) {
                    $this->Flash->success(__('The Content has been updated.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The Content could not be updated. Please, try again.'));
                }              
            }             
        }
        
        
        $this->LoadModel('Goals');
        $this->viewBuilder()->layout('admin');
        $goals = $this->paginate($this->Goals);
        $this->set(compact('goals','homecontent'));
        $this->set('_serialize', ['goals']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Run id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $this->viewBuilder()->layout('admin');
        $content = $this->Goals->get($id, [ 'contain' => [] ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            //pr($this->request->data); exit;
            $flag = true;
            if($this->request->data['title'] == ""){
                $this->Flash->error(__('Title can not be null. Please, try again.')); $flag = false;
            }
            
            if($flag){
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png');
                if (!empty($this->request->data['image']['name'])) {
                    $file = $this->request->data['image']; //put the data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $fileName = time() . "." . $ext;
                    if (in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'easydownload' . DS . $fileName);
                        $this->request->data['image'] = $fileName;
                    } else {
                        $flag = false;
                        $this->Flash->error(__('Upload image only jpg,jpeg,png files.'));
                    }
                } else {
                    unset($this->request->data['image']);
                }                
            }                
            
            if($flag){             
               
                $content = $this->Goals->patchEntity($content, $this->request->data);
                if ($this->Goals->save($content)) {
                    $this->Flash->success(__('The Content has been updated.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The Content could not be updated. Please, try again.'));
                }              
            }             
        }
        $this->set(compact('content'));
        $this->set('_serialize', ['content']);
    }    
    
    
    
    
    
    /**
     * View method
     *
     * @param string|null $id Run id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null){
        $this->viewBuilder()->layout('admin');
        $run = $this->Runs->get($id, [
            'contain' => ['Addresses', 'Orders']
        ]);

        $this->set('run', $run);
        $this->set('_serialize', ['run']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add(){
        $this->viewBuilder()->layout('admin');
        $run = $this->Goals->newEntity();
        //$run = "";
        if ($this->request->is('post')) {
            $run = $this->Goals->patchEntity($run, $this->request->data);
            $flag = true;
            if($this->request->data['title'] == ""){
                $this->Flash->error(__('Please enter title')); $flag = false;
            }
            
            
            if($flag){
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png');
                if (!empty($this->request->data['image']['name'])) {
                    $file = $this->request->data['image']; //put the data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $fileName = time() . "." . $ext;
                    if (in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . 'easydownload' . DS . $fileName);
                        $this->request->data['image'] = $fileName;
                    } else {
                        $flag = false;
                        $this->Flash->error(__('Upload image only jpg,jpeg,png files.'));
                    }
                } else {
                   $this->Flash->error(__('Please upload an image.'));
                   $flag = false;
                }                
            }  
            
            if($flag){
                $run = $this->Goals->newEntity();
                $patched = $this->Goals->patchEntity($run, $this->request->data);
                if ($this->Goals->save($patched)) {
                    $this->Flash->success(__('Your data saved successfully.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Data could not be saved. Please, try again.'));
                }
            }
        }
        $this->set(compact('run'));
        $this->set('_serialize', ['run']);
    }





    /**
     * Delete method
     *
     * @param string|null $id Run id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $run = $this->Goals->get($id);
        if ($this->Goals->delete($run)) {
            $this->Flash->success(__('Row has been deleted.'));
        } else {
            $this->Flash->error(__('Row could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
