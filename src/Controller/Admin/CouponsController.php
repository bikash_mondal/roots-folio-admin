<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

use Cake\Mailer\Email;
use Cake\Routing\Router;

use Cake\I18n\FrozenDate;
use Cake\Database\Type; 
Type::build('date')->setLocaleFormat('yyyy-MM-dd');

/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 */
class CouponsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    /*
      public function beforeFilter(Event $event) {
      if (!$this->request->session()->check('Auth.Admin')) {
      return $this->redirect(
      ['controller' => 'Coupons', 'action' => 'index']
      );
      }
      }

     */

    /*
     *  Coupons Listing
     */
    public function index() {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Coupons');
        
        #$this->paginate = ['conditions' => ['utype' => 'U']];

        //$this->set('disciplines', $this->paginate($this->Coupons));
        $query = $this->Coupons->find();
        $doctors = $this->paginate($query);
        
        
        
        
        $this->set(compact('doctors'));
        $this->set('_serialize', ['doctors']);
    }

    /**
     * View method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {

        $this->viewBuilder()->layout('admin');
        $doctors = $this->Coupons->get($id);

        //$results = $customer->toArray(); pr($results); exit;

        $this->set('doctors', $doctors);
        $this->set('_serialize', ['doctors']);
    }

    /**
     * Add Admin With permissions
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {

        $this->viewBuilder()->layout('admin');
        $this->loadModel('Users');
        if(!empty($id))
        {
            $crown = $this->Coupons->get($id);
        }
        else
        {
            $crown = $this->Coupons->newEntity();
        }
        

        if ($this->request->is(['patch', 'post', 'put'])) {
                
                $crowns = $this->Coupons->patchEntity($crown, $this->request->data);
                if($this->Coupons->save($crowns))
                {
                    $this->Flash->success(__('Discount Code has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }
                else
                {
                    $this->Flash->success(__('Discount code could not be saved.'));
                }
            
        } else {
            //$doctors = $this->Users->find("all")->select(["id","first_name","last_name"])->where(['utype' => 2,'is_active'=>1])->toArray();
            
        }
       
        $this->set(compact('doctors','crown'));
        $this->set('_serialize', ['doctors']);
    }

    /**
     * Edit Admin With Permissions
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        $this->viewBuilder()->layout('admin');
        $crown = $this->Coupons->get($id, [
            'contain' => ["Users"]
        ]);
        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['modified'] = gmdate("Y-m-d h:i:s");
            $table = $this->Users->get($this->request->data['user_id'], [
        ]);
            $user = $this->Users->patchEntity($table, $this->request->data);
            if ($this->Users->save($user)) {
                $crown = $this->Coupons->patchEntity($crown, $this->request->data);
                $this->Coupons->save($crown);                
                $this->Flash->success(__('The Crown Fabrication has been  updated.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Coupons detail could not be update. Please, try again.'));
            }
        } else {
            
            $this->request->data = $crown->toArray();
            $doctors = $this->Users->find("all")->select(["id","first_name","last_name"])->where(['utype' => 2,'is_active'=>1])->toArray();
        }

        
        
        $this->set(compact('crown','menus',"doctors"));
        $this->set('_serialize', ['crown','doctors']);
    }
    public function uploads($id=null) 
    {
       $this->viewBuilder()->layout('admin');
       $this->loadModel("Crownfabricimages");
       $crown = $this->Coupons->get($id, [
            'contain' => ["Crownfabricimages"]
        ]);
       $table = $this->Crownfabricimages->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            
            if(!empty($this->request->data['image']['name'])){
            $pathpart=pathinfo($this->request->data['image']['name']);
            $ext=$pathpart['extension'];
            $extensionValid = array('jpg','jpeg','png','gif');
            if(in_array(strtolower($ext),$extensionValid)){
            $uploadFolder = "crownimg";
            $uploadPath = WWW_ROOT . $uploadFolder;	
            $filename =uniqid().'.'.$ext;
            $full_flg_path = $uploadPath . '/' . $filename;
            move_uploaded_file($this->request->data['image']['tmp_name'],$full_flg_path);
            $this->request->data['image'] = $filename;
            }
            else{
            $this->Session->setFlash(__('Invalid image type.'));
            return $this->redirect(array('action' => 'index'));	
            }
            }
            else
            {
                $this->request->data['image']="";
            }
            
           $crowns = $this->Crownfabricimages->patchEntity($table, $this->request->data);
           if($this->Crownfabricimages->save($crowns))
           {
                $this->Flash->success(__('The Image  has been  uploaded successfully.'));
                return $this->redirect(['action' => 'index']);
           }
           

            
        }
        else
        {
            $this->request->data = $crown->toArray();
            //pr($this->request->data);exit;
        }             
              
        $this->set(compact('crown','menus',"doctors"));
        $this->set('_serialize', ['crown','doctors']);
        
        
    }
    /**
     * Delete method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    
    public function delete($id = null) {
        //$this->request->allowMethod(['post', 'delete']);
        $doctor = $this->Coupons->get($id);
        if ($this->Coupons->delete($doctor)) {
            $this->Flash->success(__('Coupons has been deleted.'));
        } else {
            $this->Flash->error(__('Coupons could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function delete_photo($id = null) {
        //$this->request->allowMethod(['post', 'delete']);
        $this->loadModel("Crownfabricimages");
        $doctor = $this->Crownfabricimages->get($id);
        if ($this->Crownfabricimages->delete($doctor)) {
            echo "1";
        } else {
            echo "0";
        }
        exit;
        
    }
    
    

    public function addresslist($id = null) {
        $this->viewBuilder()->layout('admin');
        //echo $id; exit;
        //$this->loadModel('Addrs'); $addr = $this->Addrs->find()->where(['customer_id' => $id]);
        //$adresses = TableRegistry::get('Adresses'); $adresses->find('all');

        $address = $this->Customers->Addresses->find()->contain(['Runs', 'Customers'])->where(['customer_id' => $id]);


        //$results = $address->toArray(); pr($results); exit;
        $address = $this->paginate($address);
        //$results = $address->toArray();
        //echo $id; pr($results); exit;

        $this->set(compact('address'));
        $this->set('_serialize', ['address']);
    }
    
    /*
     *  Change Admin Status
     */
}