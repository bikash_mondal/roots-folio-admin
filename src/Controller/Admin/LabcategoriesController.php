<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

use Cake\Mailer\Email;
use Cake\Routing\Router;

use Cake\I18n\FrozenDate;
use Cake\Database\Type; 
Type::build('date')->setLocaleFormat('yyyy-MM-dd');

/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 */
class LabcategoriesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    /*
      public function beforeFilter(Event $event) {
      if (!$this->request->session()->check('Auth.Admin')) {
      return $this->redirect(
      ['controller' => 'Labcategories', 'action' => 'index']
      );
      }
      }

     */

    /*
     *  Labcategories Listing
     */
    public function index() {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Labcategories');
        
        #$this->paginate = ['conditions' => ['utype' => 'U']];

        //$this->set('disciplines', $this->paginate($this->Labcategories));
        $query = $this->Labcategories->find();
        $categories = $this->paginate($query);
        
        
        
        
        $this->set(compact('categories'));
        $this->set('_serialize', ['categories']);
    }

    /**
     * View method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {

        $this->viewBuilder()->layout('admin');
        $doctors = $this->Labcategories->get($id);

        //$results = $customer->toArray(); pr($results); exit;

        $this->set('doctors', $doctors);
        $this->set('_serialize', ['doctors']);
    }

    /**
     * Add Admin With permissions
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        $this->viewBuilder()->layout('admin');
        $labcat = $this->Labcategories->newEntity();

        if ($this->request->is('post')) {
            $this->request->data['created'] = gmdate("Y-m-d h:i:s");
            $this->request->data['modified'] = gmdate("Y-m-d h:i:s"); 
            $cats = $this->Labcategories->patchEntity($labcat, $this->request->data);
            $count = $this->Labcategories->find()->where(['name'=>$this->request->data["name"]])->count();
            if(!$count)
            {
            if ($result=$this->Labcategories->save($cats)) {
                $this->Flash->success(__('The Lab category has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The Lab category could not be saved. Please, try again.'));
            }
            }
            else
            {
                $this->Flash->error(__('Duplicate category name not allowed . Please, try again.'));
            }
        } 
       
        $this->set(compact('doctors','labcat'));
        $this->set('_serialize', ['doctors']);
    }

    /**
     * Edit Admin With Permissions
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        $this->viewBuilder()->layout('admin');
        $labcat = $this->Labcategories->get($id, [
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['modified'] = gmdate("Y-m-d h:i:s");
            $count = $this->Labcategories->find()->where(['name'=>$this->request->data["name"],"id !="=>$this->request->data['id']])->count();

            $table = $this->Labcategories->get($this->request->data['id'], [
        ]);
            
            if(!$count)
            {
            $labcat = $this->Labcategories->patchEntity($table, $this->request->data);
            if ($this->Labcategories->save($labcat)) {     
                $this->Flash->success(__('The Lab category has been  updated.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Lab category detail could not be update. Please, try again.'));
            }
            }
            else
            {
             $this->Flash->error(__('Duplicate category name not allowed . Please, try again.'));

            }
        } else {
            
            $this->request->data = $labcat->toArray();
        }

        
        
        $this->set(compact('labcat','menus',"categories"));
        $this->set('_serialize', ['labcat','categories']);
    }
   
    /**
     * Delete method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    
    
        public function catstatus($id = null, $status = null) {
        //echo $id; echo "--"; echo $status; //exit;
        $this->loadModel('Labcategories'); 
        $tableRegObj = TableRegistry::get('Labcategories');
        $query = $tableRegObj->find('all', [ 'conditions' => ['id' => $id]]);
        $row = $query->first()->toArray();
        //pr($row); exit;
        if($row){
            $subquestion = TableRegistry::get('Labcategories');
            $query = $subquestion->query();
            if($status == 1){
                $query->update()->set(['is_active' => 1])->where(['id' => $id])->execute();
                $this->Flash->success(__('Category has been activated.'));
            } else if($status == 0){
                $query->update()->set(['is_active' => 0])->where(['id' => $id])->execute(); 
                $this->Flash->success(__('Category has been suspended.'));
            }
        } else {
            $this->Flash->error(__('Category Not Found.'));
        }    
        $this->redirect( Router::url( $this->referer(), true ) );
        //return $this->redirect(['action' => 'listdoctor']);
    }     

    
    public function delete($id = null) {
        //$this->request->allowMethod(['post', 'delete']);
        $doctor = $this->Labcategories->get($id);
        if ($this->Labcategories->delete($doctor)) {
            $this->Flash->success(__('Lab category has been deleted.'));
        } else {
            $this->Flash->error(__('Lab category could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    
    
    /*
     *  Change Admin Status
     */
}