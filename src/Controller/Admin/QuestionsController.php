<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

use Cake\I18n\FrozenDate;
use Cake\Database\Type; 
Type::build('date')->setLocaleFormat('yyyy-MM-dd'); 


/**
 * Admin Questions Controller
 *
 * @property \App\Model\Table\QuestionssTable $Customers
 */
class QuestionsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    /*
      public function beforeFilter(Event $event) {
      if (!$this->request->session()->check('Auth.Admin')) {
      return $this->redirect(
      ['controller' => 'Users', 'action' => 'index']
      );
      }
      }

     */

    /*
     * Questions Listing In Admin
     */
    public function index() {
        $this->viewBuilder()->layout('admin');
        $conditions=array();
        if(!empty($_REQUEST['question']))
        {
            $conditions[]['Questions.question LIKE']='%'.$_REQUEST['question'].'%';
        } 
         $this->paginate = [
        'conditions' => $conditions
    ];
        $question = $this->paginate($this->Questions);
        
        $this->set(compact('question'));
        $this->set('_serialize', ['question']);
    }

    /**
     * View Question Details
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $this->viewBuilder()->layout('admin');
        $questions = $this->Questions->get($id,['contain' => ['QuestionCheckboxes']])->toarray();
        
        //pr($questions); exit;
        //$results = $customer->toArray(); pr($results); exit;
        $this->set('questions', $questions);
        $this->set('_serialize', ['questions']);
    }

    /**
     * Add new Question
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        $this->viewBuilder()->layout('admin');
        $question = $this->Questions->newEntity();
        if ($this->request->is('post')) {
            $tableRegObj = TableRegistry::get('Questions');
            $questionExist = $tableRegObj->find()->where(['question' => $this->request->data['question']])->toArray();
            $flag = true;

            if ($this->request->data['question'] == "") {
                $this->Flash->error(__('Quetion can not be null. Please, try again.')); $flag = false;
            }
            /*
            if($flag){
                if ($this->request->data['answer'] == "") {
                    $this->Flash->error(__('Answer can not be null. Please, try again.')); $flag = false;
                }                
            }
            */
            
            if($flag){
                if ($this->request->data['answer_type'] == "") {
                    $this->Flash->error(__('Answer Type can not be null. Please, try again.')); $flag = false;
                }               
            }            

            if ($questionExist) {
                $this->Flash->error(__('Quetion already Exist. Please, change some text to make it unique.')); $flag = false;
            }
            if ($flag) {
                
                $this->request->data['added_on']=gmdate('Y-m-d H:i:s');

                $question = $this->Questions->patchEntity($question, $this->request->data);
                
                if ($this->Questions->save($question)) {
                    $this->Flash->success(__('Questions has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Questions could not be saved. Please, try again.'));
                }
            }
        }
        $this->set(compact('question'));
        $this->set('_serialize', ['question']);
    }

    /**
     * Edit & Update Question Details
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        $this->viewBuilder()->layout('admin');
        $question = $this->Questions->get($id,['contain' => []]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            //pr($this->request->data); exit;
            
            
            $tableRegObj = TableRegistry::get('Questions');
            $questionExist = $tableRegObj->find()->where(['question' => $this->request->data['question'], 'id !=' => $id])->toArray();
            $flag = true;
            if ($this->request->data['question'] == "") {
                $this->Flash->error(__('Quetion can not be null. Please, try again.')); $flag = false;
            }
            /*
            if($flag){
                if ($this->request->data['answer'] == "") {
                    $this->Flash->error(__('Answer can not be null. Please, try again.')); $flag = false;
                }                
            }
            */
            if($flag){
                if ($this->request->data['answer_type'] == "") {
                    $this->Flash->error(__('Answer Type can not be null. Please, try again.')); $flag = false;
                }               
            }  
            
            if ($questionExist) {
                $this->Flash->error(__('Quetion already Exist. Please, change some text to make it unique.')); $flag = false;
            }
            $this->request->data['updated_on']=gmdate('Y-m-d H:i:s');
            if ($flag) {
                $question = $this->Questions->patchEntity($question, $this->request->data);
                if ($this->Questions->save($question)) {
                    $this->Flash->success(__('Questions has been updated.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Questions could not be updated. Please, try again.'));
                }
            }            
        }

        $this->set(compact('question'));
        $this->set('_serialize', ['question']);
    }

    /**
     * Delete Question
     *
     * @param string|null $id Customer id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        //$this->request->allowMethod(['post', 'delete']);
        $question = $this->Questions->get($id);
        if ($this->Questions->delete($question)) {
            $this->loadModel('QuestionCheckboxes');
            $this->QuestionCheckboxes->query()->delete()->where(['qid' => $id])->execute();
            
            $this->loadModel('MedicineQuestions');
            $this->MedicineQuestions->query()->delete()->where(['qid' => $id])->execute();            

            $this->Flash->success(__('Questions has been deleted.'));
        } else {
            $this->Flash->error(__('Questions could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    
    /*
     * Add Subquestion for Question
     */
    public function subquestion( $id =null ) {

        $this->viewBuilder()->layout('admin');
        
        $this->loadModel('QuestionCheckboxes'); 
        $subquestion = $this->QuestionCheckboxes->newEntity();
        
        $question = $this->Questions->get($id,['contain' => ['QuestionCheckboxes']]);
        //pr($question->toArray()); exit;
        
        if($question->answer_type != 'c'){
            $this->Flash->error(__('Quetion have no permission to add sub question.'));
            return $this->redirect(['action' => 'index']);
        }
        
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            //pr($this->request->data); exit;
            
            $tableRegObj = TableRegistry::get('QuestionCheckboxes');
            $subquestionExist = $tableRegObj->find()->where(['title' => $this->request->data['title'], 'qid' => $this->request->data['qid']])->toArray();
            $flag = true;
            if ($this->request->data['title'] == "") {
                $this->Flash->error(__('Sub Quetion Title can not be null. Please, try again.')); $flag = false;
            }
            if ($subquestionExist) {
                $this->Flash->error(__('Sub Quetion Title already Exist. Please, change some text to make it unique.')); $flag = false;
            }
            if ($flag) {
                $subquestion = $this->QuestionCheckboxes->patchEntity($subquestion, $this->request->data);
                if ($this->QuestionCheckboxes->save($subquestion)) {
                    $this->Flash->success(__('Questions has been updated.'));
                    return $this->redirect(['action' => 'subquestion',$id]);
                } else {
                    $this->Flash->error(__('Questions could not be updated. Please, try again.'));
                    
                }
            }            
        }

        $this->set(compact('question','subquestion'));
        $this->set('_serialize', ['question','subquestion']);
        
    }    
    
    /*
     * Assign Treatments For Questions
     */
    public function treatments( $id =null ) {

        $this->viewBuilder()->layout('admin');
        
        $this->loadModel('TreatmentQuestions');
        $this->loadModel('Treatments');
        $id = base64_decode($id);
        
        $question = $this->Questions->get($id);
        if(empty($question))
        {
            $this->Flash->error(__('Questions not found. Please, try again.'));
            return $this->redirect(['action' => 'index']);
        }
        
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            //pr($this->request->data); //exit;
            $queExist = $this->TreatmentQuestions->find()->where(['qid' => $this->request->data['qid'], 'tid' => $this->request->data['tid']])->toArray();
            
            $flag = true;
            
            if ($this->request->data['tid'] == "") {
                $this->Flash->error(__('Please Select Treatment.')); $flag = false;
            }            
            
            if($flag) {
                if ($queExist) {
                    $this->Flash->error(__('Question already Exist With this Treatment. Please, choose another.')); $flag = false;
                }
            }
            
            if($flag) {
                $this->request->data['added_on'] = gmdate('Y-m-d H:i:s');
                $treatmentquestions = $this->TreatmentQuestions->newEntity();
                $treatmentquestions = $this->TreatmentQuestions->patchEntity($treatmentquestions, $this->request->data);
                //pr($treatmentquestions); exit;
                if ($this->TreatmentQuestions->save($treatmentquestions)) {
                    $this->Flash->success(__('Question has been added.'));
                    return $this->redirect(['action' => 'treatments',  base64_encode($id)]);
                } else {
                    $this->Flash->error(__('Question could not be added. Please, try again.'));
                }
            }            
        }
        
        $treatments = $this->Treatments->find('list',['fields' => ['id','name'],'conditions' => ['is_active' => 1],'order' => ['name']])->toArray();
        
        $treatmentQuestions = TableRegistry::get('TreatmentQuestions');
        $questions = $treatmentQuestions->find('all')->where(['qid' => $id])->contain(['Questions','Treatments'])->toArray();
        
        $this->set(compact('questions','question','treatmentQuestions','treatments'));
        $this->set('_serialize', ['questions','question','treatmentQuestions','treatments']);
        
    }
    
    /*
     * Delete Treatment for Questions
     */
    public function treatmentdelete($tid = null, $id = null) {
        
        $this->loadModel('TreatmentQuestions'); 
        $question = $this->TreatmentQuestions->get($id);
        if ($this->TreatmentQuestions->delete($question)) {
            $this->Flash->success(__('Treatment has been deleted.'));
        } else {
            $this->Flash->error(__('Treatment could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'treatments',base64_encode($tid)]);
        
    }
    
    /*
     * Change Treatment Status
     */
    public function treatmentstatus($tid = null, $id = null, $status = null) {
        
        $this->loadModel('TreatmentQuestions'); 
        $tableRegObj = TableRegistry::get('TreatmentQuestions');
        //$subquestionExist = $tableRegObj->find()->where(['id' => $sqid, 'qid' => $qid])->toArray();
        //echo $subquestionExist->
        //pr($subquestionExist[0]->toArray()); exit;
        $query = $tableRegObj->find('all', [ 'conditions' => ['id' => $id]]);
        $row = $query->first()->toArray();
        //pr($row); exit;
        if($row){
            $subquestion = TableRegistry::get('TreatmentQuestions');
            $query = $subquestion->query();
            if($status == 1){
                $query->update()->set(['is_active' => 1])->where(['id' => $id])->execute();
                $this->Flash->success(__('Treatment Questions has been activated.'));
            } else if($status == 0){
                $query->update()->set(['is_active' => 0])->where(['id' => $id])->execute(); 
                $this->Flash->success(__('Treatment Questions has been suspended.'));
            }
        } else {
            $this->Flash->error(__('Treatment Questions Not Found.'));
        }
        
        return $this->redirect(['action' => 'treatments',  base64_encode($tid)]);
        
    }
    
    /*
     * Delete Sub Questions
     */
    public function subqdelete($qid = null, $sqid = null) {
        
        $this->loadModel('QuestionCheckboxes'); 
        $subquestion = $this->QuestionCheckboxes->get($sqid);
        if ($this->QuestionCheckboxes->delete($subquestion)) {
            $this->Flash->success(__('Sub Question has been deleted.'));
        } else {
            $this->Flash->error(__('Sub Question could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'subquestion',$qid]);
        
    }
    
    /*
     * Change Sub Question Status
     */
    public function subqstatus($qid = null, $sqid = null, $status = null) {
        
        $this->loadModel('QuestionCheckboxes'); 
        $tableRegObj = TableRegistry::get('QuestionCheckboxes');
        //$subquestionExist = $tableRegObj->find()->where(['id' => $sqid, 'qid' => $qid])->toArray();
        //echo $subquestionExist->
        //pr($subquestionExist[0]->toArray()); exit;
        $query = $tableRegObj->find('all', [ 'conditions' => ['id' => $sqid, 'qid' => $qid]]);
        $row = $query->first()->toArray();
        //pr($row); exit;
        if($row){
            $subquestion = TableRegistry::get('QuestionCheckboxes');
            $query = $subquestion->query();
            if($status == 1){
                $query->update()->set(['is_active' => 1])->where(['id' => $sqid])->execute();
                $this->Flash->success(__('Sub Question has been activated.'));
            } else if($status == 0){
                $query->update()->set(['is_active' => 0])->where(['id' => $sqid])->execute(); 
                $this->Flash->success(__('Sub Question has been suspended.'));
            }
        } else {
            $this->Flash->error(__('Sub Question Not Found.'));
        }
        
        return $this->redirect(['action' => 'subquestion',$qid]);
        
    }    
    
    
    
    /*
     * Change Question Status
     */
    public function qstatus($id = null, $status = null) {
        //echo $id; echo "--"; echo $status; //exit;
        $this->loadModel('Questions'); 
        $tableRegObj = TableRegistry::get('Questions');
        $query = $tableRegObj->find('all', [ 'conditions' => ['id' => $id]]);
        $row = $query->first()->toArray();
        //pr($row); exit;
        if($row){
            $subquestion = TableRegistry::get('Questions');
            $query = $subquestion->query();
            if($status == 1){
                $query->update()->set(['is_active' => 1])->where(['id' => $id])->execute();
                $this->Flash->success(__('Questions has been activated.'));
            } else if($status == 0){
                $query->update()->set(['is_active' => 0])->where(['id' => $id])->execute(); 
                $this->Flash->success(__('Questions has been suspended.'));
            }
        } else {
            $this->Flash->error(__('Medicine Questions Not Found.'));
        }        
        return $this->redirect(['action' => 'index']);
    }
    
    /*
     * Treatment List For Questions
     */
    function treatmentquestion($id)
    {
        $this->loadModel('TreatmentQuestions');
        $this->loadModel('Treatments'); 
        $tableRegObj = TableRegistry::get('TreatmentQuestions');        
        $Questions = $tableRegObj->find()->where(['qid' => $id])->toArray();
        $treatment = array();
        foreach($Questions as $Question)
        {
                    $tableRegObj1 = TableRegistry::get('Treatments');
                    $q = $tableRegObj1->find()->where(['id' =>$Question->tid])->first()->toArray(); 
                    $treatment[$q['id']]=$q['name'];


        }
//        if(!empty($treatment))
//        {
//            $treatment_name= implode(",", $treatment);
//        }
//        else 
//        {
//          $treatment_name= "";
//
//        }
        $this->response->body($treatment);
        return $this->response;
        
        
        
    }
    
    
    
    
}
