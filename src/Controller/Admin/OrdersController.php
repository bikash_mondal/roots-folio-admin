<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController {

    
    
    public function initialize() {
        parent::initialize();
        //$conn = ConnectionManager::get('default');
        //$this->loadComponent('Paginator');

    }  
    public $components = ['Paginator'];
    public $paginate = ['limit' => 15];
    
    public function beforeFilter(Event $event) {
        if (!$this->request->session()->check('Auth.Admin')) {
            return $this->redirect(['controller' => 'Users', 'action' => 'index']);
        }
    }

    /*
     *  Pending Order Listing
     */
    public function index() {

        $this->viewBuilder()->layout('admin');
        
        $this->loadModel('Orders');
        $query = $this->Orders->find()->where(['is_reject' => 0,'isverified' => 0,'is_delivered' => 0,'is_complete' => 1,'is_active' => 1,'transaction_id !=' => '' ])->order(['id' => 'DESC'])->all()->toArray(); 
        
        $uniqueDt = array();
        foreach($query as $q){
            $uniqueDt[$q->transaction_id]['id'] = $q->id;
        }

        $dtArr = array();
        foreach($uniqueDt as $dt){
            $dtArr[$dt['id']] = $dt['id'];
        }
        if(empty($dtArr)){ $dtArr[0] = 0; }
        $this->set('orders', $this->Paginator->paginate($this->Orders, [ 'limit' => 10, 'order' => [ 'id' => 'DESC' ], 'conditions' => [ 'id IN' => $dtArr ]]));
          
        $this->set(compact('dtArr'));

    }

    /*
     *  Pending orders listing
     */
    public function approvedorders() {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Orders');
        $query = $this->Orders->find()->where(['is_reject' => 0,'isverified' => 1,'is_delivered' => 0,'is_complete' => 1,'is_active' => 1,'transaction_id !=' => '' ])->order(['id' => 'DESC'])->all()->toArray(); 
        $uniqueDt = array();
        foreach($query as $q){
            $uniqueDt[$q->transaction_id]['id'] = $q->id;
        }
        $dtArr = array();
        foreach($uniqueDt as $dt){
            $dtArr[$dt['id']] = $dt['id'];
        }
        if(empty($dtArr)){ $dtArr[0] = 0; }            
            
            
        $this->set('orders', $this->Paginator->paginate($this->Orders, [ 'limit' => 10, 'order' => [ 'id' => 'DESC' ], 'conditions' => [ 'id IN' => $dtArr ]]));

        $this->set(compact('dtArr'));

    }   
    
    /*
     *  Rejected  orders
     */
    public function rejectedorders() {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Orders');
        $query = $this->Orders->find()->where(['is_reject' => 1,'isverified' => 0,'is_delivered' => 0,'is_complete' => 1,'is_active' => 1,'transaction_id !=' => '' ])->order(['id' => 'DESC'])->all()->toArray(); 
        $uniqueDt = array();
        foreach($query as $q){
            $uniqueDt[$q->transaction_id]['id'] = $q->id;
        }
        $dtArr = array();
        foreach($uniqueDt as $dt){
            $dtArr[$dt['id']] = $dt['id'];
        }
        if(empty($dtArr)){ $dtArr[0] = 0; }  
        $this->set('orders', $this->Paginator->paginate($this->Orders, [ 'limit' => 10, 'order' => [ 'id' => 'DESC' ], 'conditions' => [ 'id IN' => $dtArr ]]));

        $this->set(compact('dtArr'));

    }    
    
    
    /**
     * View Order Details
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($txn = null) {

        $this->viewBuilder()->layout('admin');
        $this->loadModel('Admins');
        $uid = $this->request->session()->read('Auth.Admin.id');
        $user = $this->Admins->get($this->request->session()->read('Auth.Admin.id'));

        $this->loadModel('Treatments');
        $this->loadModel('Orders');
        $this->loadModel('Orderdetails');
        $this->loadModel('Medicines');
        $this->loadModel('Pils');
        $this->loadModel('Prescriptions');
        $this->loadModel('Transactions');

        $orderExist = $this->Orders->find()->contain(['Orderdetails','Treatments','Orderdetails.Medicines'])->where(['Orders.transaction_id' => $txn])->all()->toArray();
        $presc = $this->Prescriptions->find()->where(['Prescriptions.txnid' => $txn])->all()->toArray();

        foreach($orderExist as $oExist){ $dt = json_decode($oExist['question']); }
        
        if($this->request->is('post')){
            if($this->request->data['ftype'] == 'reject'){
                //pr($this->request->data); exit;
                $transaction = $this->Transactions->find()->contain(['Orders','Users'])->where(['Transactions.transaction_id' => $this->request->data['transid']])->first()->toArray();
                $request_params = array(
                    'METHOD' => 'RefundTransaction',
                    'TRANSACTIONID' => $this->request->data['transid'],
                    'REFUNDTYPE' => 'Full',
                    'USER' => 'nits.debsoumen2_api1.gmail.com',
                    'PWD' => 'LJMGQ33VJGFUZGXQ',
                    'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AjUTZMwQl04R5EwZTojhK76GG1wR',
                    'VERSION' => '85.0',
                    'CURRENCYCODE' => 'GBP',
                );
                $nvp_string = '';
                foreach ($request_params as $var => $val) {
                    $nvp_string .= '&' . $var . '=' . urlencode($val);
                }
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_VERBOSE, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($curl, CURLOPT_TIMEOUT, 3000);
                curl_setopt($curl, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);

                $result = curl_exec($curl);
                curl_close($curl);
                parse_str($result, $resArray);

                if($resArray['ACK'] == 'Success' ){
                    $retTransactionId = $resArray['REFUNDTRANSACTIONID'];
                    $refAmt = $resArray['NETREFUNDAMT']; 
                    $record_id = $transaction['id'];
                    $trans['is_reject'] = 1;
                    $trans['reject_by'] = "Admin";
                    $trans['reject_by_id'] = $this->request->session()->read('Auth.Admin.id');
                    $trans['is_refunded'] = 1;
                    $trans['refund_amt'] = $refAmt;
                    $trans['refund_transaction_id'] = $retTransactionId;
                    $trans['reasion'] = $this->request->data['data'];
                    $transactionTable = TableRegistry::get('Transactions');
                    $query1 = $transactionTable->query();
                    $query1->update()->set($trans)->where(['id' => $record_id])->execute();

                    foreach($transaction['Orders'] as $cancelOrder){
                        $record_ids = $cancelOrder['id'];
                        $order['is_reject'] = 1;
                        $order['reject_by'] = "Admin";
                        $order['reject_by_id'] = $this->request->session()->read('Auth.Admin.id');
                        $order['is_refunded'] = 1;
                        $order['refund_amt'] = $refAmt;
                        $order['refund_transaction_id'] = $retTransactionId;
                        $order['reasion'] = $this->request->data['data'];
                        $ordersTable = TableRegistry::get('Orders');
                        $query = $ordersTable->query();
                        $query->update()->set($order)->where(['id' => $record_ids])->execute();                
                    }
                    $this->Flash->success(__('Order Rejected Successfully.'));
                    $this->redirect(['controller' => 'Orders', 'action' => 'view',$txn]);                
                    
                } else {
                    $this->Flash->success(__('Order Not Rejected. Try again'));
                    $this->redirect(['controller' => 'Orders', 'action' => 'view',$txn]);
                }                

            }
            
            if($this->request->data['ftype'] == 'msg'){
                
                
                //pr($this->request->data); exit;
                
                
                $this->loadModel('Ordermsgs');
                $ordermsgsTable = TableRegistry::get('Ordermsgs');
                $ordermsg = $ordermsgsTable->newEntity(); 
                $ordermsg->ordid = $this->request->data['transid'];
                $ordermsg->fromid = $this->request->data['fromid'];
                $ordermsg->toid = $this->request->data['toid'];
                $ordermsg->msg = $this->request->data['msg'];
                $ordermsg->pid = $this->request->data['pid'];
                $ordermsg->type = $this->request->data['type'];              
                $ordermsg->fromtype = $this->request->data['fromtype'];
                $ordermsg->totype = $this->request->data['totype'];
                $ordermsg->date = gmdate('Y-m-d H:i:s');               
                if ($ordermsgsTable->save($ordermsg)){ $id = $ordermsg->id; }                  
                $this->Flash->success(__('Message Sent Successfully.'));
            }
        }
         
        //pr($user); pr($orderExist); exit;
        
        $this->set(compact('orderExist','user','presc'));
        $this->set('_serialize', ['user']); 


    }

    /*
     *  Approve order as Admin
     */
    public function approvenow($txn = null) {
        
        $this->viewBuilder()->layout('');
        $this->loadModel('Users');
        $this->loadModel('Orders');
        $this->loadModel('Transactions');
        $this->loadModel('Admins');
        
        $user = $this->Admins->get($this->request->session()->read('Auth.Admin.id'));
        
        if ($txn != "") {
            
            $transaction = $this->Transactions->find()->contain(['Orders','Users'])->where(['Transactions.transaction_id' => $txn])->first()->toArray();
            //$ord = $this->Orders->find()->where(['Orders.transaction_id' => $txn])->all()->toArray();
            
            $record_id = $transaction['id'];
            $transactionTable = TableRegistry::get('Transactions');
            $query1 = $transactionTable->query();
            $query1->update()->set(['isverified' => 1, 'verifiedby' => $user['id'], 'verifiedbytype' => 'Admin'])->where(['id' => $record_id])->execute(); 

            foreach($transaction['Orders'] as $cancelOrder){ 
                $record_ids = $cancelOrder['id'];
                $ordersTable = TableRegistry::get('Orders');
                $query = $ordersTable->query();
                $query->update()->set(['isverified' => 1, 'verifiedby' => $user['id'], 'verifiedbytype' => 'Admin'])->where(['id' => $record_ids])->execute();                   
            }            
             
             $this->Flash->success(__('Prescription Approved successfully.'));
             return $this->redirect(['action' => 'approvedorders']);
             
        } else {
            return $this->redirect(['action' => 'view', $txn]);
        }
            
        $this->autoRender = false;
    } 
    
    /*
     *  order Statistics
     */
    public function statistics() 
    {
    
     $this->loadModel('Orders');
     $query=$this->Orders->find();
     $transaction = $query->select(['sum' => $query->func()->sum('amt'),'total_order'=>$query->func()->count('id')])->where(['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>''])->first()->toArray();
     $this->set(compact('transaction'));
     $this->set('_serialize', ['transaction']); 
    
    }
    
    /*
     *  Order Chart Statistics
     */
    public function orderanalytic() 
    {
    
     $this->loadModel('Orders');
     $query=$this->Orders->find();
     $transaction = $query->select(['sum' => $query->func()->sum('amt'),'total_order'=>$query->func()->count('id')])->where(['is_reject'=>0,'isverified'=>1])->first()->toArray();
     $this->set(compact('transaction'));
     $this->set('_serialize', ['transaction']); 
    
    }
    
    /*
     *  Order Chart search by Day Month And Year with time Duration
     */
    public function chart_type() 
    {
             
        $ip= isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? 
        $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];         
        $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip)); 
        $result=array();
        if($data->status == 'success')
        {
             $timezone = $data->timezone;
        }
        else 
        {
            $timezone="Asia/Kolkata";
        }
        $flag=0;
        date_default_timezone_set($timezone);
        $GMT = new \DateTimeZone('UTC');
        $type=$this->request->data['type'];
        $start1=explode("/",$this->request->data['start_date']);
        $this->request->data['start_date']=$start1[2].'-'.$start1[1].'-'.$start1[0];
        $end1=explode("/",$this->request->data['end_date']);
        $this->request->data['end_date']=$end1[2].'-'.$end1[1].'-'.$end1[0];
        $start_date=date('Y-m-d 00:00:00',strtotime($this->request->data['start_date']));
        $end_date=date("Y-m-d 23:59:59",strtotime($this->request->data['end_date']));
        $start=new \DateTime($start_date);
        $start->setTimezone($GMT);
        $start_time=$start->format('Y-m-d H:i:s');
        $end=new \DateTime($end_date);
        $end->setTimezone($GMT);
        $end_time=$end->format('Y-m-d H:i:s');
        $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));
        $order_history=array();
        if($type=='daily')
        {
            
            $listings = $this->Orders->find()->select(['id','name','email','amt','date'])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$end_time]
                        )->contain(['Orderdetails'])->order(['date'=>'desc'])->all()->toArray();
            
                       
                foreach ($listings as $list)
                {
                  $order_date=date('Y-m-d h:i',strtotime($list->date));
                  $gmtTimezone = new \DateTimeZone('GMT'); 
                  $myDateTime = new \DateTime($order_date, $gmtTimezone);
                  $offset = date("P");
                  $qty=count($list->orderdetails);
                  $place_order_date=date('d/m/Y h:i a', $myDateTime->format('U') + $offset);
                  $order_history[]=array('id'=>$list->id,'name'=>$list->name,'email'=>$list->email,'amt'=>number_format((float)$list->amt,2,'.',','),'date'=>$place_order_date,"qty"=>$qty);  
                }
            
            
           
            while($start_time<$end_time)
            {
                
                $query=$this->Orders->find();
                $transaction = $query->select(['sum' => $query->func()->sum('amt'),'total_order'=>$query->func()->count('id')])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$initital_end]
                        )->first()->toArray();
                
                $userTimezone = new \DateTimeZone($timezone);
                $gmtTimezone = new \DateTimeZone('GMT'); 
                $myDateTime = new \DateTime($start_time, $gmtTimezone);
                $offset = $userTimezone->getOffset($myDateTime);
                $x[]=date('d/m/Y', $myDateTime->format('U') + $offset);
                $y[]=!empty($transaction['sum'])?$transaction['sum']:0;
                if($transaction['sum']>0)
                {
                    $flag=1;
                }
                $start_time=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));;
                $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));  

            }
            
            
            
            
            
            
            $result=array('x'=>$x,'y'=>$y,'flag'=>$flag,'order_history'=>$order_history);
            
        }
        
        if($type=='monthly')
        {
            $i=0;
            $month=array("01"=>'Jan',"02"=>'Feb',"03"=>'March',"04"=>'April',"05"=>'May',"06"=>'June',"07"=>'July',
            "08"=>'Aug',"09"=>'Sep',"10"=>'Oct',"11"=>'Nov',"12"=>'Dec'    
            );
            
            $listings = $this->Orders->find()->select(['id','name','email','amt','date'])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$end_time]
                        )->contain(['Orderdetails'])->order(['date'=>'desc'])->all()->toArray();
            
                       
                foreach ($listings as $list)
                {
                  $order_date=date('Y-m-d h:i',strtotime($list->date));
                  $gmtTimezone = new \DateTimeZone('GMT'); 
                  $myDateTime = new \DateTime($order_date, $gmtTimezone);
                  $offset = date("P");
                  $qty=count($list->orderdetails);
                  $place_order_date=date('d/m/Y h:i a', $myDateTime->format('U') + $offset);
                  $order_history[]=array('id'=>$list->id,'name'=>$list->name,'email'=>$list->email,'amt'=>number_format((float)$list->amt,2,'.',','),'date'=>$place_order_date,"qty"=>$qty);  
                }
            
            
            $datetime1 = new \DateTime($start_time);
            $datetime2 = new \DateTime($end_time);
            $difference = $datetime1->diff($datetime2);
            $no_of_days=$difference->days;
            while($i<=$no_of_days)
            {
                
                $query=$this->Orders->find();
                $transaction = $query->select(['sum' => $query->func()->sum('amt'),'total_order'=>$query->func()->count('id')])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$initital_end]
                        )->first()->toArray();
                
                $userTimezone = new \DateTimeZone($timezone);
                $gmtTimezone = new \DateTimeZone('GMT'); 
                $myDateTime = new \DateTime($start_time, $gmtTimezone);
                $offset = $userTimezone->getOffset($myDateTime);
                $d=date('d-m-Y', $myDateTime->format('U') + $offset);
                $monthname=$month[date('m',strtotime($d))].'-'.date('y',strtotime($d));
                $monthly[$monthname][]=!empty($transaction['sum'])?$transaction['sum']:0;
                if($transaction['sum']>0)
                {
                    $flag=1;
                }
                $start_time=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));;
                $initital_end=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));  
                $i++;
            
                
            }
            
            foreach($monthly as $month=> $rec)
            {
                $x[]=$month;
                $y[]= array_sum($rec);
            }
            
            
            
            
            
            
           $result=array('x'=>$x,'y'=>$y,'flag'=>$flag,'order_history'=>$order_history);
            
        }
        
         if($type=='yearly')
        {
            $i=0;
            $datetime1 = new \DateTime($start_time);
            $datetime2 = new \DateTime($end_time);
            $difference = $datetime1->diff($datetime2);
            $no_of_days=$difference->days;
            $listings = $this->Orders->find()->select(['id','name','email','amt','date'])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$end_time]
                        )->contain(['Orderdetails'])->order(['date'=>'desc'])->all()->toArray();
            
                       
                foreach ($listings as $list)
                {
                  $order_date=date('Y-m-d h:i',strtotime($list->date));
                  $gmtTimezone = new \DateTimeZone('GMT'); 
                  $myDateTime = new \DateTime($order_date, $gmtTimezone);
                  $offset = date("P");
                  $qty=count($list->orderdetails);
                  $place_order_date=date('d/m/Y h:i a', $myDateTime->format('U') + $offset);
                  $order_history[]=array('id'=>$list->id,'name'=>$list->name,'email'=>$list->email,'amt'=> number_format((float)$list->amt,2,'.',','),'date'=>$place_order_date,"qty"=>$qty);  
                }
            while($i<=$no_of_days)
            {
                
                $query=$this->Orders->find();
                $transaction = $query->select(['sum' => $query->func()->sum('amt'),'total_order'=>$query->func()->count('id')])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$initital_end]
                        )->first()->toArray();
                
                $userTimezone = new \DateTimeZone($timezone);
                $gmtTimezone = new \DateTimeZone('GMT'); 
                $myDateTime = new \DateTime($start_time, $gmtTimezone);
                $offset = $userTimezone->getOffset($myDateTime);
                $d=date('d-m-Y', $myDateTime->format('U') + $offset);
                $monthname='Since-'.date('Y',strtotime($d));
                $monthly[$monthname][]=!empty($transaction['sum'])?$transaction['sum']:0;
                if($transaction['sum']>0)
                {
                    $flag=1;
                }
                $start_time=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));;
                $initital_end=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));  
                $i++;
            
                
            }
            
            foreach($monthly as $month=> $rec)
            {
                $x[]=$month;
                $y[]= array_sum($rec);
            }
            
            
           $result=array('x'=>$x,'y'=>$y,'flag'=>$flag,'order_history'=>$order_history);
            
        }
       echo json_encode($result);exit;

    }
    
    /*
     *  order count Chart with Search
     */
    public function chart_ordergraph() 
    {
             
        $ip= isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? 
        $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']; 
        
        $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip)); 
        $result=array();
        if($data->status == 'success')
        {
             $timezone = $data->timezone;
        }
        else 
        {
            $timezone="Asia/Kolkata";
        }
        //$timezone="Asia/Kolkata";
        $flag=0;
        date_default_timezone_set($timezone);
        $GMT = new \DateTimeZone('UTC');
        $type=$this->request->data['type'];
        $start1=explode("/",$this->request->data['start_date']);
        $this->request->data['start_date']=$start1[2].'-'.$start1[1].'-'.$start1[0];
        $end1=explode("/",$this->request->data['end_date']);
        $this->request->data['end_date']=$end1[2].'-'.$end1[1].'-'.$end1[0];
        $start_date=date('Y-m-d 00:00:00',strtotime($this->request->data['start_date']));
        $end_date=date("Y-m-d 23:59:59",strtotime($this->request->data['end_date']));
        $start=new \DateTime($start_date);
        $start->setTimezone($GMT);
        $start_time=$start->format('Y-m-d H:i:s');
        $end=new \DateTime($end_date);
        $end->setTimezone($GMT);
        $end_time=$end->format('Y-m-d H:i:s');
        $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));
        $order_history=array();
        if($type=='daily')
        {
            
            $listings = $this->Orders->find()->select(['id','name','email','amt','date'])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$end_time]
                        )->contain(['Orderdetails'])->order(['date'=>'desc'])->all()->toArray();
            
                       
                foreach ($listings as $list)
                {
                  $order_date=date('Y-m-d h:i',strtotime($list->date));
                  $gmtTimezone = new \DateTimeZone('GMT'); 
                  $myDateTime = new \DateTime($order_date, $gmtTimezone);
                  $offset = date("P");
                  $qty=count($list->orderdetails);
                  $place_order_date=date('d/m/Y h:i a', $myDateTime->format('U') + $offset);
                  $order_history[]=array('id'=>$list->id,'name'=>$list->name,'email'=>$list->email,'amt'=> number_format((float)$list->amt,2,'.',','),'date'=>$place_order_date,"qty"=>$qty);  
                }
            
            
           
            while($start_time<$end_time)
            {
                
                $query=$this->Orders->find();
                $transaction = $query->select(['sum' => $query->func()->sum('amt'),'total_order'=>$query->func()->count('id')])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$initital_end]
                        )->first()->toArray();
                
                $userTimezone = new \DateTimeZone($timezone);
                $gmtTimezone = new \DateTimeZone('GMT'); 
                $myDateTime = new \DateTime($start_time, $gmtTimezone);
                $offset = $userTimezone->getOffset($myDateTime);
                $x[]=date('d/m/Y', $myDateTime->format('U') + $offset);
                $y[]=!empty($transaction['total_order'])?$transaction['total_order']:0;
                if($transaction['total_order']>0)
                {
                    $flag=1;
                }
                $start_time=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));;
                $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));  

            }
            
            $result=array('x'=>$x,'y'=>$y,'flag'=>$flag,'order_history'=>$order_history);
            
        }
        
        if($type=='monthly')
        {
            $i=0;
            $month=array("01"=>'Jan',"02"=>'Feb',"03"=>'March',"04"=>'April',"05"=>'May',"06"=>'June',"07"=>'July',
            "08"=>'Aug',"09"=>'Sep',"10"=>'Oct',"11"=>'Nov',"12"=>'Dec'    
            );
            
            $listings = $this->Orders->find()->select(['id','name','email','amt','date'])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$end_time]
                        )->contain(['Orderdetails'])->order(['date'=>'desc'])->all()->toArray();
            
                       
                foreach ($listings as $list)
                {
                  $order_date=date('Y-m-d h:i',strtotime($list->date));
                  $gmtTimezone = new \DateTimeZone('GMT'); 
                  $myDateTime = new \DateTime($order_date, $gmtTimezone);
                  $offset = date("P");
                  $qty=count($list->orderdetails);
                  $place_order_date=date('d/m/Y h:i a', $myDateTime->format('U') + $offset);
                  $order_history[]=array('id'=>$list->id,'name'=>$list->name,'email'=>$list->email,'amt'=> number_format((float)$list->amt,2,'.',','),'date'=>$place_order_date,"qty"=>$qty);  
                }
            
            $datetime1 = new \DateTime($start_time);
            $datetime2 = new \DateTime($end_time);
            $difference = $datetime1->diff($datetime2);
            $no_of_days=$difference->days;
            while($i<=$no_of_days)
            {
                
                $query=$this->Orders->find();
                $transaction = $query->select(['sum' => $query->func()->sum('amt'),'total_order'=>$query->func()->count('id')])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$initital_end]
                        )->first()->toArray();
                
                $userTimezone = new \DateTimeZone($timezone);
                $gmtTimezone = new \DateTimeZone('GMT'); 
                $myDateTime = new \DateTime($start_time, $gmtTimezone);
                $offset = $userTimezone->getOffset($myDateTime);
                $d=date('d-m-Y', $myDateTime->format('U') + $offset);
                $monthname=$month[date('m',strtotime($d))].'-'.date('y',strtotime($d));
                $monthly[$monthname][]=!empty($transaction['total_order'])?$transaction['total_order']:0;
                if($transaction['total_order']>0)
                {
                    $flag=1;
                }
                $start_time=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));;
                $initital_end=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));  
                $i++;
            
                
            }
            
            foreach($monthly as $month=> $rec)
            {
                $x[]=$month;
                $y[]= array_sum($rec);
            }
            
            
            
            
            
            
           $result=array('x'=>$x,'y'=>$y,'flag'=>$flag,'order_history'=>$order_history);
            
        }
        
         if($type=='yearly')
        {
            $i=0;
            $datetime1 = new \DateTime($start_time);
            $datetime2 = new \DateTime($end_time);
            $difference = $datetime1->diff($datetime2);
            $no_of_days=$difference->days;
            $listings = $this->Orders->find()->select(['id','name','email','amt','date'])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$end_time]
                        )->contain(['Orderdetails'])->order(['date'=>'desc'])->all()->toArray();
            
                       
                foreach ($listings as $list)
                {
                  $order_date=date('Y-m-d h:i',strtotime($list->date));
                  $gmtTimezone = new \DateTimeZone('GMT'); 
                  $myDateTime = new \DateTime($order_date, $gmtTimezone);
                  $offset = date("P");
                  $qty=count($list->orderdetails);
                  $place_order_date=date('d/m/Y h:i a', $myDateTime->format('U') + $offset);
                  $order_history[]=array('id'=>$list->id,'name'=>$list->name,'email'=>$list->email,'amt'=> number_format((float)$list->amt,2,'.',','),'date'=>$place_order_date,"qty"=>$qty);  
                }
            while($i<=$no_of_days)
            {
                
                $query=$this->Orders->find();
                $transaction = $query->select(['sum' => $query->func()->sum('amt'),'total_order'=>$query->func()->count('id')])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$initital_end]
                        )->first()->toArray();
                
                $userTimezone = new \DateTimeZone($timezone);
                $gmtTimezone = new \DateTimeZone('GMT'); 
                $myDateTime = new \DateTime($start_time, $gmtTimezone);
                $offset = $userTimezone->getOffset($myDateTime);
                $d=date('d-m-Y', $myDateTime->format('U') + $offset);
                $monthname='Since-'.date('Y',strtotime($d));
                $monthly[$monthname][]=!empty($transaction['total_order'])?round($transaction['total_order']):0;
                if($transaction['total_order']>0)
                {
                    $flag=1;
                }
                $start_time=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));;
                $initital_end=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));  
                $i++;
            
                
            }
            
            foreach($monthly as $month=> $rec)
            {
                $x[]=$month;
                $y[]= array_sum($rec);
            }
            
            
           $result=array('x'=>$x,'y'=>$y,'flag'=>$flag,'order_history'=>$order_history);
            
        }
       echo json_encode($result);exit;
    }
    
//    public function allorders(){$this->viewBuilder()->layout('admin');$this->loadModel('Orders');$this->loadModel('Users');$users = $this->Orders->find('all',['fields' => ['Users.id','Users.first_name','Users.last_name']])->where(['Orders.user_id != ' => 0])->contain(['Users'])->order(['Users.first_name' => 'ASC'])->group(['Orders.user_id'])->toArray();$query = $this->Orders->find()->where(['is_complete' => 1,'is_active' => 1,'transaction_id !=' => '' ])->order(['id' => 'DESC'])->all()->toArray();$uniqueDt = array();foreach($query as $q){$uniqueDt[$q->transaction_id]['id'] = $q->id;}$dtArr = array();foreach($uniqueDt as $dt){$dtArr[$dt['id']] = $dt['id'];}if(empty($dtArr)){ $dtArr[0] = 0; }$conditions['id IN'] = $dtArr;if(!empty($_REQUEST)){if(!empty($_REQUEST['user_id'])){$conditions['user_id'] = $_REQUEST['user_id'];}if(!empty($_REQUEST['transaction_id'])){$conditions['transaction_id LIKE '] = '%'.$_REQUEST['transaction_id'].'%';}if(!empty($_REQUEST['start_date'])){$conditions['date(date) >='] = date('Y-m-d',strtotime($_REQUEST['start_date']));}if(!empty($_REQUEST['end_date'])){$conditions['date(date) <='] = date('Y-m-d',strtotime($_REQUEST['end_date']));}if(!empty($_REQUEST['export'])){$export_data = $this->Orders->find('all',['conditions' => [$conditions], 'contain' => ['Orderdetails'],'order' => [ 'id' => 'DESC' ]])->toArray();$output = '';$output.="#,";$output.="Patient,";$output.="Transaction Id,";$output.="Pills,";$output.="Amount,";$output.="Order Date,";$output.="Shipping Address,";$output.="Status,";$output .="\n";$ik = 1;foreach ($export_data as $order){$output.='"' . $ik . '",';$output.='"' . $order->name . '",';$output.='"' . $order->transaction_id . '",';$output.='"' . count($order->orderdetails) . '",';$output.='"' . $order->amt . '",';$output.='"' . $this->requestAction('admin/users/change_datetimeformat/'.strtotime($order->date)) . '",';$output.='"' . $order->shipping_address .',' . $order->shipping_city .','. $order->shipping_country . ',' . $order->shipping_code .'",';if($order->is_reject == 0 && $order->isverified==1){$output.="Approved";}else if($order->is_reject == 1 && $order->isverified==0){$output.="Rejected";}else if($order->is_reject == 0 && $order->isverified==0){$output.="Pending";}$output .="\n";$ik++;}$filename = "order_".time().".csv";header('Content-type: application/csv');header('Content-Disposition: attachment; filename='.$filename);echo $output;exit;}}$this->set('orders', $this->Paginator->paginate($this->Orders, [ 'limit' => 30, 'order' => [ 'id' => 'DESC' ], 'conditions' => [ $conditions ], 'contain' => ['Orderdetails']]));$this->set(compact('dtArr','users'));}
    
    /*
     *  Export Orders By Conditions
     */
        public function allorders()
        {
	$this->viewBuilder()->layout('admin');
	$this->loadModel('Orders');
	$this->loadModel('Users');
	$users = $this->Orders->find('all', ['fields' => ['Users.id', 'Users.first_name', 'Users.last_name']])->where(['Orders.user_id != ' => 0])->contain(['Users'])->order(['Users.first_name' => 'ASC'])->group(['Orders.user_id'])->toArray();
	$query = $this->Orders->find()->where(['is_complete' => 1, 'is_active' => 1, 'transaction_id !=' => ''])->order(['id' => 'DESC'])->all()->toArray();
	$uniqueDt = array();
	foreach($query as $q)
		{
		$uniqueDt[$q->transaction_id]['id'] = $q->id;
		}

	$dtArr = array();
	foreach($uniqueDt as $dt)
		{
		$dtArr[$dt['id']] = $dt['id'];
		}

	if (empty($dtArr))
		{
		$dtArr[0] = 0;
		}

	$conditions['id IN'] = $dtArr;
	if (!empty($_REQUEST))
		{
		if (!empty($_REQUEST['user_id']))
			{
			$conditions['user_id'] = $_REQUEST['user_id'];
			}

		if (!empty($_REQUEST['transaction_id']))
			{
			$conditions['transaction_id LIKE '] = '%' . $_REQUEST['transaction_id'] . '%';
			}

		if (!empty($_REQUEST['start_date']))
			{
			$conditions['date(date) >='] = date('Y-m-d', strtotime($_REQUEST['start_date']));
			}

		if (!empty($_REQUEST['end_date']))
			{
			$conditions['date(date) <='] = date('Y-m-d', strtotime($_REQUEST['end_date']));
			}

		if (!empty($_REQUEST['export']))
			{
			$export_data = $this->Orders->find('all', ['conditions' => [$conditions], 'contain' => ['Orderdetails'], 'order' => ['id' => 'DESC']])->toArray();
			$output = '';
			$output.= "#,";
			$output.= "Patient,";
			$output.= "Transaction Id,";
			$output.= "Pills,";
			$output.= "Amount,";
			$output.= "Order Date,";
			$output.= "Shipping Address,";
			$output.= "Status,";
			$output.= "\n";
			$ik = 1;
			foreach($export_data as $order)
				{
				$output.= '"' . $ik . '",';
				$output.= '"' . $order->name . '",';
				$output.= '"' . $order->transaction_id . '",';
				$output.= '"' . count($order->orderdetails) . '",';
				$output.= '"' . $order->amt . '",';
				$output.= '"' . $this->requestAction('admin/users/change_datetimeformat/' . strtotime($order->date)) . '",';
				$output.= '"' . $order->shipping_address . ',' . $order->shipping_city . ',' . $order->shipping_country . ',' . $order->shipping_code . '",';
				if ($order->is_reject == 0 && $order->isverified == 1)
					{
					$output.= "Approved";
					}
				  else
				if ($order->is_reject == 1 && $order->isverified == 0)
					{
					$output.= "Rejected";
					}
				  else
				if ($order->is_reject == 0 && $order->isverified == 0)
					{
					$output.= "Pending";
					}

				$output.= "\n";
				$ik++;
				}

			$filename = "order_" . time() . ".csv";
			header('Content-type: application/csv');
			header('Content-Disposition: attachment; filename=' . $filename);
			echo $output;
			exit;
			}
		}

	$this->set('orders', $this->Paginator->paginate($this->Orders, ['limit' => 30, 'order' => ['id' => 'DESC'], 'conditions' => [$conditions], 'contain' => ['Orderdetails']]));
	$this->set(compact('dtArr', 'users'));
	}
        
        public function salesreport()
        {
        header('Content-type: text/plain');
	$this->viewBuilder()->layout('admin');
	$this->loadModel('Orders');
	$this->loadModel('Users');
        $this->loadModel('Pils');
        $this->loadModel('Medicines');
        $this->loadModel('Orderdetails');
        $conditions['Orders.is_active']=1;
        $conditions['Orders.is_reject']=0;
        $conditions['Orders.isverified']=1;
        $conditions['Orders.is_complete']=1;
        $conditions['Orders.transaction_id !=']="";
        
        if(!empty($_REQUEST['title']))
        {
          $conditions['Pils.title LIKE']='%'.$_REQUEST['title'].'%';

        }
        
        if(!empty($_REQUEST['start_date']))
        {
            $conditions['DATE(Orders.date) >= ']= date('Y-m-d',strtotime($_REQUEST['start_date']));
        }
        
        if(!empty($_REQUEST['end_date']))
        {
            $conditions['DATE(Orders.date) <= ']= date('Y-m-d',strtotime($_REQUEST['end_date']));
        }
        
        $pills= $this->Paginator->paginate($this->Orderdetails, ['limit' => 30, 'order' => ['id' => 'DESC'], 'conditions' => [$conditions], 'contain' => ['Pils','Orders','Medicines'],"group"=>"pil_id"]);
        $output="";
        if (!empty($_REQUEST['export']))
        {
            $output.="#,";
            $output.="Pill,";
            $output.="Medicine,";
            $output.="Qty,";
            $output.="Amount,";
            $output.="\n";
            $ik = 1;
            $export_data = $this->Orderdetails->find('all', ['conditions' => [$conditions], 'contain' => ['Pils','Orders','Medicines'], 'order' => ['Orderdetails.id' => 'DESC'],'group'=>'pil_id'])->toArray();
            
            foreach($export_data as $result)
            {
                $priceqty=$this->business($result->pil_id);
                $price=explode("-",$priceqty);
                $output.='"' . str_replace('"', '""',$ik ) . '", ';
                $output.='"' . str_replace('"', '""', $result->pil_name) . '", ';
                $output.='"' . str_replace('"', '""', $result->medicine->title) . '", ';
                $output.='"' . str_replace('"', '""', $price[0]) . '", ';
                $output.='"' . str_replace('"', '""', number_format((float)$price[1],2,'.',',')) . '", ';
                $output .="\n";
                $ik++;

            }
            
            $filename = "sales".time().".csv";
            header('Content-type: application/csv');
            header('Content-Disposition: attachment; filename='.$filename);
            echo $output;
            exit;

            
          
        }
        $this->set(compact('dtArr', 'pills'));
        
	}
        
        public function business($pill_id)
        {
	$this->loadModel('Orders');
	$this->loadModel('Users');
        $this->loadModel('Pils');
        $this->loadModel('Medicines');
        $this->loadModel('Orderdetails');
        $query=$this->Orderdetails->find();
        $conditions['Orders.is_active']=1;
        $conditions['Orders.is_reject']=0;
        $conditions['Orders.isverified']=1;
        $conditions['Orders.is_complete']=1;
        $conditions['Orders.transaction_id !=']="";
        $conditions['Orderdetails.pil_id']=$pill_id;
        $net_qty=0;
        $sum=0;
        $export_data = $this->Orderdetails->find('all', ['conditions' => [$conditions], 'contain' => ['Orders']])->toArray();
        foreach($export_data as  $data)
        {
             $net_qty=$net_qty+1;
             $sum=$sum+$data->pil_price;
             
        }
        
       
        $this->response->body($net_qty.'-'.$sum);
        return $this->response;
        
        
	}
        
        /*
         *  Profit Graph with search
         */
        public function profit_analytic() 
        {
            $this->loadModel('Orders');
            $this->loadModel('Orderdetails');
            $conditions['Orders.is_active']=1;
            $conditions['Orders.is_complete']=1;
            $conditions['Orders.transaction_id !=']="";
            $conditions['Orders.is_reject']=0;
            $conditions['Orders.isverified']=1;
             $sum=0;
             $transactions=$this->Orders->find('all', ['conditions' => [$conditions], 'contain' => ['Orderdetails'=>['Pils']]])->toArray(); ;
             foreach ($transactions as $tr)
             {
                 
                 
                 foreach ($tr->orderdetails as $order_details)
                 {
                 $profit=$order_details->pil_price-$order_details->pil->buy_price;
                 $sum=$sum+$profit;
                 }
                 
             }
             $this->set(compact('sum'));
             $this->set('_serialize', ['sum']);
        }
        
       public function profit_graph() 
        {      
        $ip= isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? 
        $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']; 
        $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip)); 
        $result=array();
        if($data->status == 'success')
        {
             $timezone = $data->timezone;
        }
        else 
        {
            $timezone="Asia/Kolkata";
        }
        $flag=0;
        date_default_timezone_set($timezone);
        $GMT = new \DateTimeZone('UTC');
        $type=$this->request->data['type'];
        $start1=explode("/",$this->request->data['start_date']);
        $this->request->data['start_date']=$start1[2].'-'.$start1[1].'-'.$start1[0];
        $end1=explode("/",$this->request->data['end_date']);
        $this->request->data['end_date']=$end1[2].'-'.$end1[1].'-'.$end1[0];
        $start_date=date('Y-m-d 00:00:00',strtotime($this->request->data['start_date']));
        $end_date=date("Y-m-d 23:59:59",strtotime($this->request->data['end_date']));
        $start=new \DateTime($start_date);
        $start->setTimezone($GMT);
        $start_time=$start->format('Y-m-d H:i:s');
        $end=new \DateTime($end_date);
        $end->setTimezone($GMT);
        $end_time=$end->format('Y-m-d H:i:s');
        $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));
        $order_history=array();
        if($type=='daily')
        {
            
            $listings = $this->Orders->find()->select(['id','name','email','amt','date'])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$end_time]
                        )->contain(['Orderdetails'=>['Pils']])->order(['date'=>'desc'])->all()->toArray();
            
                       
                foreach ($listings as $list)
                {
                  $order_date=date('Y-m-d h:i',strtotime($list->date));
                  $gmtTimezone = new \DateTimeZone('GMT'); 
                  $myDateTime = new \DateTime($order_date, $gmtTimezone);
                  $offset = date("P");
                  $qty=count($list->orderdetails);
                  $place_order_date=date('d/m/Y h:i a', $myDateTime->format('U') + $offset);
                  $net_profit=0;
                  $net_sellingprice=0;
                  $net_costing=0;
                  foreach ($list->orderdetails as $dtl)
                 {
                    $profit=$dtl->pil_price-$dtl->pil->buy_price;
                    $net_profit=$net_profit+$profit;
                    $net_sellingprice=$net_sellingprice+$dtl->pil_price;
                    $net_costing=$net_costing+$dtl->pil->buy_price;
                    
                 }
                  $order_history[]=array('id'=>$list->id,'net_profit'=>  number_format((float)$net_profit,2,'.',','),'net_sellingprice'=>  number_format((float)$net_sellingprice,2,'.',','),'net_costing'=>  number_format((float)$net_costing,2,'.',','),'name'=>$list->name,'email'=>$list->email,'amt'=> number_format((float)$list->amt,2,'.',','),'date'=>$place_order_date,"qty"=>$qty);  
                }
                while($start_time<$end_time)
                {

                    $conditions['is_active']=1;
                    $conditions['is_complete']=1;
                    $conditions['transaction_id !=']="";
                    $conditions['is_reject']=0;
                    $conditions['isverified']=1;
                    $conditions['date >=']=$start_time;
                    $conditions['date <=']=$initital_end;
                    $transactions=$this->Orders->find('all', ['conditions' => [$conditions], 'contain' => ['Orderdetails'=>['Pils']]])->toArray(); ;
                    $sum=0;
                    foreach ($transactions as $tr)
                    {
                        foreach ($tr->orderdetails as $order_details)
                        {
                           
                        $profit=$order_details->pil_price-$order_details->pil->buy_price ;
                        $sum=$sum+$profit;
                        $flag=1;
                        }
                        
                        
                    }
                   
                    $userTimezone = new \DateTimeZone($timezone);
                    $gmtTimezone = new \DateTimeZone('GMT'); 
                    $myDateTime = new \DateTime($start_time, $gmtTimezone);
                    $offset = $userTimezone->getOffset($myDateTime);
                    $x[]=date('d/m/Y', $myDateTime->format('U') + $offset);
                    $y[]=!empty($sum)?number_format((float)$sum,2,'.',','):0;

                    $start_time=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));;
                    $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));  

                }
                $result=array('x'=>$x,'y'=>$y,'flag'=>$flag,'order_history'=>$order_history);
            
        }
        
        if($type=='monthly')
        {
            $i=0;
            $month=array("01"=>'Jan',"02"=>'Feb',"03"=>'March',"04"=>'April',"05"=>'May',"06"=>'June',"07"=>'July',
            "08"=>'Aug',"09"=>'Sep',"10"=>'Oct',"11"=>'Nov',"12"=>'Dec'    
            );
            
            $listings = $this->Orders->find()->select(['id','name','email','amt','date'])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$end_time]
                        )->contain(['Orderdetails'=>['Pils']])->order(['date'=>'desc'])->all()->toArray();
            
                       
                foreach ($listings as $list)
                {
                  $order_date=date('Y-m-d h:i',strtotime($list->date));
                  $gmtTimezone = new \DateTimeZone('GMT'); 
                  $myDateTime = new \DateTime($order_date, $gmtTimezone);
                  $offset = date("P");
                  $qty=count($list->orderdetails);
                  $place_order_date=date('d/m/Y h:i a', $myDateTime->format('U') + $offset);
                  $net_profit=0;
                  $net_sellingprice=0;
                  $net_costing=0;
                  foreach ($list->orderdetails as $dtl)
                 {
                    $profit=$dtl->pil_price-$dtl->pil->buy_price;
                    $net_profit=$net_profit+$profit;
                    $net_sellingprice=$net_sellingprice+$dtl->pil_price;
                    $net_costing=$net_costing+$dtl->pil->buy_price;
                    
                 }
                  $order_history[]=array('id'=>$list->id,'net_profit'=>  number_format((float)$net_profit,2,'.',','),'net_sellingprice'=>  number_format((float)$net_sellingprice,2,'.',','),'net_costing'=>  number_format((float)$net_costing,2,'.',','),'name'=>$list->name,'email'=>$list->email,'amt'=> number_format((float)$list->amt,2,'.',','),'date'=>$place_order_date,"qty"=>$qty);  
                }
            
            
            $datetime1 = new \DateTime($start_time);
            $datetime2 = new \DateTime($end_time);
            $difference = $datetime1->diff($datetime2);
            $no_of_days=$difference->days;
            while($i<=$no_of_days)
            {
                $sum=0;
                $conditions['is_active']=1;
                $conditions['is_complete']=1;
                $conditions['transaction_id !=']="";
                $conditions['is_reject']=0;
                $conditions['isverified']=1;
                $conditions['date >=']=$start_time;
                $conditions['date <=']=$initital_end;
                $transactions=$this->Orders->find('all', ['conditions' => [$conditions], 'contain' => ['Orderdetails'=>['Pils']]])->toArray(); ;
                foreach ($transactions as $tr)
                {


                    foreach ($tr->orderdetails as $order_details)
                    {
                    $profit=$order_details->pil_price-$order_details->pil->buy_price ;
                    $sum=$sum+$profit;
                    $flag=1;
                    }
                }
                
                $userTimezone = new \DateTimeZone($timezone);
                $gmtTimezone = new \DateTimeZone('GMT'); 
                $myDateTime = new \DateTime($start_time, $gmtTimezone);
                $offset = $userTimezone->getOffset($myDateTime);
                $d=date('d-m-Y', $myDateTime->format('U') + $offset);
                $monthname=$month[date('m',strtotime($d))].'-'.date('y',strtotime($d));
                $monthly[$monthname][]=!empty($sum)? $sum:0;
               
                $start_time=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));;
                $initital_end=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));  
                $i++;
            
                
            }
            
            foreach($monthly as $month=> $rec)
            {
                $x[]=$month;
                $y[]= array_sum($rec);
            }
            
            
            
            
            
            
           $result=array('x'=>$x,'y'=>$y,'flag'=>$flag,'order_history'=>$order_history);
            
        }
        
         if($type=='yearly')
        {
            $i=0;
            $datetime1 = new \DateTime($start_time);
            $datetime2 = new \DateTime($end_time);
            $difference = $datetime1->diff($datetime2);
            $no_of_days=$difference->days;
           $listings = $this->Orders->find()->select(['id','name','email','amt','date'])->where(
                        ['is_active'=>1,'is_reject'=>0,'isverified'=>1,"is_complete"=>1,"transaction_id !="=>'',"date >="=>$start_time,"date <="=>$end_time]
                        )->contain(['Orderdetails'=>['Pils']])->order(['date'=>'desc'])->all()->toArray();
            
                       
                foreach ($listings as $list)
                {
                  $order_date=date('Y-m-d h:i',strtotime($list->date));
                  $gmtTimezone = new \DateTimeZone('GMT'); 
                  $myDateTime = new \DateTime($order_date, $gmtTimezone);
                  $offset = date("P");
                  $qty=count($list->orderdetails);
                  $place_order_date=date('d/m/Y h:i a', $myDateTime->format('U') + $offset);
                  $net_profit=0;
                  $net_sellingprice=0;
                  $net_costing=0;
                  foreach ($list->orderdetails as $dtl)
                 {
                    $profit=$dtl->pil_price-$dtl->pil->buy_price;
                    $net_profit=$net_profit+$profit;
                    $net_sellingprice=$net_sellingprice+$dtl->pil_price;
                    $net_costing=$net_costing+$dtl->pil->buy_price;
                    
                 }
                  $order_history[]=array('id'=>$list->id,'net_profit'=>  number_format((float)$net_profit,2,'.',','),'net_sellingprice'=>  number_format((float)$net_sellingprice,2,'.',','),'net_costing'=>  number_format((float)$net_costing,2,'.',','),'name'=>$list->name,'email'=>$list->email,'amt'=> number_format((float)$list->amt,2,'.',','),'date'=>$place_order_date,"qty"=>$qty);  
                }
            while($i<=$no_of_days)
            {
                
                $sum=0;
                $conditions['is_active']=1;
                $conditions['is_complete']=1;
                $conditions['transaction_id !=']="";
                $conditions['is_reject']=0;
                $conditions['isverified']=1;
                $conditions['date >=']=$start_time;
                $conditions['date <=']=$initital_end;
                $transactions=$this->Orders->find('all', ['conditions' => [$conditions], 'contain' => ['Orderdetails'=>['Pils']]])->toArray(); ;
                foreach ($transactions as $tr)
                {


                    foreach ($tr->orderdetails as $order_details)
                    {
                    $profit=$order_details->pil_price-$order_details->pil->buy_price ;
                    $sum=$sum+$profit;
                    $flag=1;
                    }
                }
                
                $userTimezone = new \DateTimeZone($timezone);
                $gmtTimezone = new \DateTimeZone('GMT'); 
                $myDateTime = new \DateTime($start_time, $gmtTimezone);
                $offset = $userTimezone->getOffset($myDateTime);
                $d=date('d-m-Y', $myDateTime->format('U') + $offset);
                $monthname='Since-'.date('Y',strtotime($d));
                $monthly[$monthname][]=!empty($sum)?$sum:0;
                
                $start_time=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));;
                $initital_end=date('Y-m-d H:i:s', strtotime("+1 days", strtotime($start_time)));  
                $i++;
            
                
            }
            
            foreach($monthly as $month=> $rec)
            {
                $x[]=$month;
                $y[]= array_sum($rec);
            }
            
            
           $result=array('x'=>$x,'y'=>$y,'flag'=>$flag,'order_history'=>$order_history);
            
        }
       echo json_encode($result);exit;
    }
    
        public function pendingorders() {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Orders');
        $conditions['is_reject']=0;
        $conditions['isverified']=1;
        $conditions['is_delivered']=0;
        $conditions['is_complete']=1;
        $conditions['is_active']=1;
        $conditions['transaction_id !=']="";
        if(!empty($_REQUEST['start_date']) && !empty($_REQUEST['end_date']))
        {
          $starts= explode("/", $_REQUEST['start_date']);
          $ends= explode("/", $_REQUEST['end_date']);
          $start_date=$starts[2].'-'.$starts[1].'-'.$starts[0];
          $end_date=$ends[2].'-'.$ends[1].'-'.$ends[0];
        $conditions['date >=']=$start_date;
        $conditions['date <=']=$end_date;
        }
        $query = $this->Orders->find()->where($conditions)->order(['id' => 'DESC'])->all()->toArray(); 
        $uniqueDt = array();
        foreach($query as $q){
            $uniqueDt[$q->transaction_id]['id'] = $q->id;
        }
        $dtArr = array();
        foreach($uniqueDt as $dt){
            $dtArr[$dt['id']] = $dt['id'];
        }
        if(empty($dtArr)){ $dtArr[0] = 0; }            
            
            
        $this->set('orders', $this->Paginator->paginate($this->Orders, [ 'limit' => 10, 'order' => [ 'id' => 'DESC' ], 'conditions' => [ 'id IN' => $dtArr ]]));
        $output="";
        if (!empty($_REQUEST['export']))
        {
            $output.="#,";
            $output.="Order From,";
            $output.="Transaction Id,";
            $output.="Order Date,";
            $output.="\n";
            $ik = 1;
            $export_data = $this->Orders->find('all', ['conditions' => [ 'id IN' => $dtArr ],'order' => ['Orders.id' => 'DESC']])->toArray();
            
            foreach($export_data as $result)
            {
                
                $output.='"' . str_replace('"', '""',$ik ) . '", ';
                $output.='"' . str_replace('"', '""', $result->name) . '", ';
                $output.='"' . str_replace('"', '""', $result->transaction_id) . '", ';
                $output.='"' . str_replace('"', '""', date('d F Y', strtotime($result->date))) . '", ';
                $output .="\n";
                $ik++;

            }
            
            $filename = "pendingDelivery".time().".csv";
            header('Content-type: application/csv');
            header('Content-Disposition: attachment; filename='.$filename);
            echo $output;
            exit;

            
          
        }
        
        
        
        
        $this->set(compact('dtArr'));

    }
    
    function details($id)
    {
     $order = $this->Orders->find()->where(['id'=>$id])->contain(['Orderdetails' => ['Pils','Medicines']])->first()->toArray(); 
     $this->set(compact('order'));
        
    }
    
    function detailpil($pill)
    {
        $this->loadModel('Orderdetails');
        $conditions['Orders.is_active']=1;
        $conditions['Orders.is_reject']=0;
        $conditions['Orders.isverified']=1;
        $conditions['Orders.is_complete']=1;
        $conditions['Orders.transaction_id !=']="";
        $conditions['Orderdetails.pil_id']=$pill;
        $orderDetails = $this->Orderdetails->find('all', ['conditions' => [$conditions], 'contain' => ['Orders'],'order'=>['Orders.date'=>'desc']])->toArray();
        $this->set(compact('orderDetails'));
        
        
    }
    
    function UpdatePils()
    {
       $this->loadModel('Pils');
       $pils = $this->Pils->find('all')->toArray();
       foreach($pils as $pil)
       {
           $rec['buy_price']=$pil->cost-5;
           $ordersTable = TableRegistry::get('Pils');
           $query = $ordersTable->query();
           
           $query->update()->set($rec)->where(['id' => $pil->id])->execute(); 
       }

    exit;            

        
    }
    
        
        
        
   
    
    
}
