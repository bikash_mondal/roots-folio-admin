<?php
namespace App\Controller\Api;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Mailer\Email;
class CurlPatternsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['categories']);
        $this->loadComponent('RequestHandler');
    }

    public function categories()
    {
      $this->loadModel('CurlCategories');
      $categories = $this->CurlCategories->find('all')->toArray();
      pr($categories);
      exit;
    }
}
