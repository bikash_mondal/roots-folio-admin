<?php
namespace App\Controller\Api;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Mailer\Email;
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['token','register','forgotpassword','details','updateuser_service','listtechnician','updateimage_service','refers','test_push','userstechnicianlist','uploads']);
        $this->loadComponent('RequestHandler');
    }

    public function add()
    {
        $this->Crud->on('afterSave', function(Event $event) {
            if ($event->subject->created) {
                $this->set('data', [
                    'id' => $event->subject->entity->id,
                    'token' => JWT::encode(
                        [
                            'sub' => $event->subject->entity->id,
                            'exp' =>  time() + 604800
                        ],
                    Security::salt())
                ]);
                $this->Crud->action()->config('serialize.data', 'data');
            }
        });
        return $this->Crud->execute();
    }

    public function token()
    {
        //pr($this->request->data);
        //exit;
        $user = $this->Auth->identify();
        if (!empty($user))
        {
          // print_r($user);
          // exit;
            if($user['status']==1)
            {
                $get_user = $this->Users->get($user['id']);
                $up_data['deviceid'] = (!empty($this->request->data['deviceid'])?$this->request->data['deviceid']:'');
                $up_data['devicetype'] = (!empty($this->request->data['devicetype'])?$this->request->data['devicetype']:'');
                $get_user = $this->Users->patchEntity($get_user, $up_data);
                $this->Users->save($get_user);
                if(empty($user['profile_image']))
                {
                    $user['profile_image'] = 'default.png';
                }
                $user['image_url'] = Router::url('/', true).'user_img/';
                $user['token'] = JWT::encode([
                            'sub' => $user['id'],
                            'exp' =>  time() + 604800
                        ],
                        Security::salt());
                $this->set([
                    'ack' => 1,
                    'details' => $user,
                    '_serialize' => ['ack', 'data','details']
                ]);
            }
            else
            {
                $this->set([
                    'ack' => 0,
                    'message' => 'Please subscribe first to login.',
                    'details' => $user,
                    '_serialize' => ['ack', 'message','details']
                ]);
            }
        }
        else
        {

            $this->set([
                'ack' => 0,
                'message' => 'Inavlid email or password.',
                '_serialize' => ['ack', 'message']
            ]);

        }

    }

    public function register()
    {
    

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
             //$tableRegObj = TableRegistry::get('Users');
            $userExist = $this->Users
                            ->find()
                            ->where(['email' => $this->request->data['email']])->first();
            $usernameExist = $this->Users
                            ->find()
                            ->where(['username' => $this->request->data['username']])->first();
            if(!empty($userExist))
            {
              $rarray = array('ack' => 0,'message' => 'Email already exist.');
            }
            else if(!empty($usernameExist)) {
              $rarray = array('ack' => 0,'message' => 'Username already exist.');
            }
            else
            {
                $this->request->data['created'] = gmdate("Y-m-d h:i:s");
                $this->request->data['modified'] = gmdate("Y-m-d h:i:s");
                $this->request->data['status'] = 1;
                //$this->request->data['utype'] = 2;
                $user = $this->Users->patchEntity($user, $this->request->data);
                if ($rs = $this->Users->save($user)) {
                    if(!empty($this->request->data['profile_pics']))
                    {
                      $this->loadModel('UserPhotos');
                      foreach($this->request->data['profile_pics'] as $photo)
                      {
                        $pic_data = array('user_id' => $rs->id, 'name' => $photo['filename']);
                        $profile_pics = $this->UserPhotos->newEntity();
                        $profile_pics = $this->UserPhotos->patchEntity($profile_pics, $pic_data);
                        $this->UserPhotos->save($profile_pics);
                      }
                    }
                    // $etRegObj = TableRegistry::get('EmailTemplates');
                    // $emailTemp = $etRegObj->find()->where(['id' => 2])->first()->toArray();
                    //
                    // $chkPost = base64_encode($rs->id . "/" . $themail);
                    // $last_id=base64_encode($rs->id);
                    // $mail_To = $themail;
                    // //$mail_CC = '';
                    // $mail_subject = $emailTemp['subject'];
                    // $url = Router::url('/', true);
                    // $link = $url . 'products/payment-new/' . $last_id;
                    //
                    // $mail_body = str_replace(array('[NAME]', '[LINK]'), array($fullname, $link), $emailTemp['content']);
                    // //echo $mail_body; //exit;
                    //
                    // //Sending user email validation link
                    // $email = new Email('default');
                    // $email->emailFormat('html')
                    //         ->to($mail_To)
                    //         ->subject($mail_subject)
                    //         ->send($mail_body);
                    $rarray = array('ack' => 1,'message' => 'Registration Successfull.','details' => $rs);
                }
                else {
                    $rarray = array('ack' => 0, 'message' => 'Internal error. Please try again later.','error' => $rs);
                }

            }
            $this->set([
                'details' => $rarray,
                '_serialize' => ['details']
            ]);
        }
    }

    public function forgotpassword()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {

            // Checking if User is valid or not.
            $tableRegObj = TableRegistry::get('Users');
            $userExist = $tableRegObj->find()->where(['email' => $this->request->data['email']])->first();

            $etRegObj = TableRegistry::get('EmailTemplates');
            $emailTemp = $etRegObj->find()->where(['id' => 1])->first()->toArray();


            if (!empty($userExist)) {
                $chkPost = rand(1000,9999); //Generating new Password

//                $userdt = TableRegistry::get('Users');
//                $query = $userdt->query();
//                $this->Users->save();
                $userExist->password = $chkPost;
                $this->Users->save($userExist);
                //$query->update()->set(['password' => $chkPost])->where(['id' => $userExist['id']])->execute();

                $mail_To = $userExist->email;
                $mail_CC = '';
                $mail_subject = $emailTemp['subject'];
                $name = $userExist->name;
                $url = Router::url('/', true);
                $link = $url . 'users/setpassword/' . $chkPost;

                $mail_body = str_replace(array('[NAME]', '[PASSWORD]'), array($name, $chkPost), $emailTemp['content']);
                //echo $mail_body; //exit;

                // Sending user the reset password link.
                $email = new Email('default');

                $email_res = $email->emailFormat('html')
                                ->to($userExist->email)
                                ->subject($mail_subject)
                                ->send($mail_body);

                if ($email_res) {

                    $this->set([
                        'ack' => 1,
                        'message' => 'Please check your email for new password.',
                        '_serialize' => ['ack', 'message']
                    ]);
                } else {
                    $this->set([
                        'ack' => 0,
                        'message' => 'Ineternal error. Please try again later.',
                        '_serialize' => ['ack', 'message']
                    ]);
                }
            } else {
                $this->set([
                    'ack' => 0,
                    'message' => 'Email Not Registerd With Us, try with another.',
                    '_serialize' => ['ack', 'message']
                ]);
            }
        }
    }

    public function details()
    {
        if($this->request->is('post'))
        {
            $user_details = $this->Users->find()->where(['Users.id' => $this->request->data['user_id']])->first();
            if(!empty($user_details))
            {
                if(empty($user_details['profile_image']))
                {
                    $user_details['profile_image'] = 'default.png';
                }
                $user_details['image_url'] = Router::url('/', true).'user_img/';
                $this->set([
                    'ack' => 1,
                    'details' => $user_details,
                    '_serialize' => ['details','ack']
                ]);
            }
            else
            {
                $this->set([
                    'ack' => 0,
                    'message' => 'User not found',
                    '_serialize' => ['ack','message']
                ]);
            }
        }
    }

    public function updateuser_service()
    {
        if($this->request->is('post'))
        {
            if(!empty($this->request->data['pass']))
            {
                $this->request->data['password'] = $this->request->data['pass'];
            }
            else {
                unset($this->request->data['password']);
            }
            $users = $this->Users->get($this->request->data['id']);
            $users = $this->Users->patchEntity($users, $this->request->data);
            $users->modified = date("Y-m-d H:i:s");
            if($this->Users->save($users))
            {
                $this->set([
                    'ack' => 1,
                    'message' => 'Profile updated successfully.',
                    '_serialize' => ['message','ack']
                ]);
            }
            else
            {
                $this->set([
                    'ack' => 0,
                    'message' => 'Internal error. Please try again later.',
                    '_serialize' => ['ack','message']
                ]);
            }
        }
    }

    public function updateimage_service(){
        if(!empty($_FILES))
        {
            $this->loadModel('Users');
            $rarray = array();
            $ext = explode('/', $_FILES['photo']['type']);
            if ($ext) {
                $uploadFolder = "user_img";
                $uploadPath = WWW_ROOT . $uploadFolder;
                $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                if (in_array($ext[1], $extensionValid)) {


                    $imageName = $_POST['user_id'] . '_' . (strtolower(trim($_FILES['photo']['name'])));
                    $full_image_path = $uploadPath . '/' . $imageName;
                    move_uploaded_file($_FILES['photo']['tmp_name'], $full_image_path);
                    $User_data['profile_image'] = $imageName;
                    $User_data['id'] = $_POST['user_id'];
                    $user = $this->Users->get($this->request->data['user_id']);
                    $crowns = $this->Users->patchEntity($user, $User_data);
                    //if($crown_fabric = $this->Crownfabrications->save($crowns))
                    /**/
                    //unlink($uploadPath. '/' .$this->request->data['User']['hidprofile_img']);

                    if($this->Users->save($crowns))
                    {
                        //$rarray = array('ack' => 1,'message' => 'Profile picture updated successfully.');
                        $this->set([
                            'ack' => 1,
                            'message' => 'Profile picture updated successfully.',
                            '_serialize' => ['message','ack']
                        ]);
                    }
                    else
                    {
                        $this->set([
                            'ack' => 0,
                            'message' => 'Not enough permission to upload.',
                            '_serialize' => ['message','ack']
                        ]);
                    }
                } else {
                    $this->set([
                            'ack' => 0,
                            'message' => 'Please upload image of .jpg, .jpeg, .png or .gif format.',
                            '_serialize' => ['message','ack']
                    ]);
                }
            }
            else {
                //$rarray = array('ack' => 0, 'message' => 'Internal error. Please try again later');
                    $this->set([
                            'ack' => 0,
                            'message' => 'Internal error. Please try again later',
                            '_serialize' => ['message','ack']
                    ]);
            }

        }
    }

    public function listtechnician( ) {
        $this->loadModel('Users');
        $users = $this->Users->find('all')->where(['Users.utype' => 3,'Users.is_active' => 1]);
        $this->set([
                    'ack' => 1,
                    'technicians' => $users,
                    '_serialize' => ['technicians','ack']
                ]);

    }
    public function userstechnicianlist( ) {
        $this->loadModel('Users');

        if($this->request->is('post'))
        {
            $user_id = $this->request->data['user_id'];
            $this->loadModel('ReferredUsers');
            $ref_users = $this->ReferredUsers->find()->where(["OR" => array("ReferredUsers.user_by" => $user_id,"ReferredUsers.user_to" => $user_id)])->all();
            $technicians = array();
            foreach($ref_users as $ref)
            {
                if($ref->user_by == $user_id)
                {
                    $technicians[] = $ref->user_to;
                }
                else {
                    $technicians[] = $ref->user_by;
                }
            }
            if(!empty($technicians))
            {
                $users = $this->Users->find('all')->where(['Users.utype' => 3,'Users.is_active' => 1,'Users.id IN' => $technicians]);
            }
            else
            {
                $users = array();
            }
            $this->set([
                        'ack' => 1,
                        'technicians' => $users,
                        '_serialize' => ['technicians','ack']
                    ]);
        }

    }

    public function refers()
    {
        if($this->request->is('post'))
        {
            $emails = $this->request->data['email'];
            $user_id = $this->request->data['user_id'];
            $emails = explode(",", $emails);
            $this->loadModel('Refers');
            $this->loadModel('EmailTemplates');
            $emailTemp = $this->EmailTemplates->find()->where(['id' => 3])->first()->toArray();
            $email_ob = new Email('default');
            $mail_subject = $emailTemp['subject'];

            $doctor = $this->Users->find()->where(['id' => $user_id])->first();
            $this->loadModel('SiteSettings');
            $settings = $this->SiteSettings->find()->first();
            $mail_body = str_replace(array('[ANDROIDLINK]', '[IOSLINK]','[DOCTORNAME]'), array($settings['androaid_link'], $settings['ios_link'],$doctor->first_name.' '.$doctor->last_name), $emailTemp['content']);
            foreach($emails as $email)
            {
                if(!empty($email))
                {
                    $user_exists = $this->Users->find()->where(['Users.email' => $email])->first();
                    if(!empty($user_exists))
                    {
                        $this->loadModel('ReferredUsers');
                        $exists_data = $this->ReferredUsers->find()->where(['OR' => array(array('ReferredUsers.user_by' => $user_id,'ReferredUsers.user_to' => $user_exists->id),
                                                                            array('ReferredUsers.user_to' => $user_id,'ReferredUsers.user_by' => $user_exists->id))])
                                                                            ->first();
                        if(empty($exists_data))
                        {
                            $newdata['user_by'] = $user_id;
                            $newdata['user_to'] = $user_exists->id;
                            $refered_data = $this->ReferredUsers->newEntity();
                            $new_refs = $this->ReferredUsers->patchEntity($refered_data, $newdata);
                            $this->ReferredUsers->save($new_refs);
                        }
                    }
                    $exists = $this->Refers->find()->where(['Refers.email' => $email,'Refers.user_id' => $user_id])->first();
                    if(empty($exists))
                    {
                        $data['email'] = $email;
                        $data['user_id'] = $user_id;
                        $refer = $this->Refers->newEntity();
                        $ref_data = $this->Refers->patchEntity($refer, $data);
                        $this->Refers->save($ref_data);
                    }






                    $email_ob->emailFormat('html')
                                    ->to($email)
                                    ->subject($mail_subject)
                                    ->send($mail_body);
                }


            }
            $this->set([
                    'ack' => 1,
                    'message' => 'Referrals sent successfully.',
                    '_serialize' => ['message','ack']
            ]);
        }
    }

    public function test_push()
    {
        //$totalmsgarray = array("message" => 'Helloo');

        $push_message = array("title" => 'PerfectShade',"message" => 'New Case Received','type' => 'newoffer','postid'=>146);

        #$this->android_push(array('d9XE0BGSdy8:APA91bFgmTyTxeuq_920aaa9_hL26B1vhQc_tqt4jlbE8LB7lc4K2olp7vA9FNgJYUtFKsGIiUUdWLpvBjwnVf4M-ydznfOkI-zkkdqh4g46FeKgJ2iz73Kf2inXRAQgnJLHPgtCPgki'), $push_message);
        //$this->android_push(array('cMoFFhdK_60:APA91bF-ki90R5fyxzW7a_NUbr3q_sPORcS3jYS3Sl8hBe88haxFIPYqt14oIJIMaZ4hOXOoYKuzLhuN11kzy8uhENFp0seIV4tgoVRt2rAhTbcK_Ghi-j-uLWZ8uXpVDayQzPoWKrUp'), $push_message);

    	$type = 'project_add';
        $pushmsg = 'hiii';
        $totalmsgarray = array("message" => $pushmsg);
        $notification_sound = 'default';
        $this->iphone_push('35f2a7404ec14e768968ec92c2edda6af918071bc1133a1fe13e5ed3bb23f0bd', 'New Offer Posted on your List','default',array('type' => 'offeraccepted'));
        #$this->iphone_push('e0ec50e9345861b82184187129ef714a426f4509b3a8a6d03135e0db875be6ab', 'Push testing by bikash','default',array('type' => 'newoffer','postid'=>146));

        exit;

    }

    public function uploads()
    {
        if ($this->request->is('post')) {
            if (!empty($this->request->data['photoimg']['name'])) {
                $pathpart = pathinfo($this->request->data['photoimg']['name']);
                $ext = $this->request->data['photoimg']['type'];
                if (!empty(strpos($ext, '/'))) {
                    $ext = explode('/', $ext);
                    $ext = $ext['1'];
                }
                $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder = 'user_img';
                    $uploadPath = WWW_ROOT.$uploadFolder;
                    $filename = uniqid().'.'.$ext;
                    $full_flg_path = $uploadPath.'/'.$filename;
                    if (move_uploaded_file($this->request->data['photoimg']['tmp_name'], $full_flg_path)) {

                        $this->set([
                            'ack' => 1,
                            'message' => 'Image uploaded successfully.',
                            'filename' => $filename,
                            'url' => Router::url('/', true).$uploadFolder.'/'.$filename.'?'.time(),
                            '_serialize' => ['ack', 'message', 'filename', 'url', 'result', 'height', 'width'],
                        ]);
                    } else {
                        $this->set([
                            'ack' => 0,
                            'message' => 'Not enough permission to upload.',
                            '_serialize' => ['ack', 'message'],
                        ]);
                    }
                } else {
                    $this->set([
                        'ack' => 0,
                        'message' => 'Invalid image type.',
                        '_serialize' => ['ack', 'message'],
                    ]);
                }
            }
        }
    }
}
