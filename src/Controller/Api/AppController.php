<?php
namespace App\Controller\Api;
use Cake\Controller\Controller;
use Cake\Event\Event;
class AppController extends Controller
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');

        $this->loadComponent('Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                'Form' => [
                    //'scope' => ['Users.is_active' => 1],
                    'fields' => [
                        'username'=>'username',
                        'password'=>'password'
                    ]
                ],
                'ADmad/JwtAuth.Jwt' => [
                    'parameter' => 'token',
                    'userModel' => 'Users',
                    //'scope' => ['Users.is_active' => 1],
                    'fields' => [
                        'username' => 'id'
                    ],
                    'queryDatasource' => true
                ]
            ],
            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize'
        ]);
    }

    public function android_push($reg_ids,$push_message) {

            //echo "subhradip";
            //$url = 'https://android.googleapis.com/qcm/send';
            $url = 'https://fcm.googleapis.com/fcm/send';

            $totalarray=array('msg'=>'hii subhradip');
            $message = array("message" => json_encode($totalarray));

            $fields = array(
                'registration_ids' =>$reg_ids,
                'data' => $push_message,
            );
           /*$fields = array(
                'registatoin_ids' =>json_encode($reg_ids),
                'data' => $push_message,
            );*/
            //print_r($fields);
            //exit;
            $headers = array(
                'Authorization: key=AAAAvlcXD_Y:APA91bE-qNkhwxbD5SC2RoGAB-PYR4hoDXa7_4N18sWpP7Duz42S8H0upxZF_b6LqKQX870d_BJSICqtd2yYzpa25DDimzFM-fRlJWsZ1HYE3FXvyZQMq70gj077k7OivTpuM6wRnFqA',
                'Content-Type: application/json'
            );
            // Open connection
            $ch = curl_init();
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            // Execute post
            $result = curl_exec($ch);
            //print_r($result);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            // Close connection
            curl_close($ch);
            //echo $result;
            //exit;
    //return true;
        }

        public function iphone_push($device_token_id,  $message, $notificationsound, $totalarray) {
            $data = array();

            $deviceToken = $device_token_id;
            $passphrase = '1234567890';
            $ctx = stream_context_create();
             //stream_context_set_option($ctx, 'ssl', 'local_cert', 'HapZarDistPush.pem'); //for Distribution  //05.04.2017
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'PerfectShadePushDist.pem'); //for dev MakeAnOfferPushDev.pem
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
         $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx); //for Distribution
//           $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx); //for dev  //05.04.2017


            if (!$fp) {
                exit("Failed to connect: $err $errstr" . PHP_EOL);
            }
            // 'Connected to APNS' . PHP_EOL;
        // Create the payload body
            $body['aps'] = array(
                'alert' => $message,
                'sound' => $notificationsound,
                'data' => $totalarray
            );
        // Encode the payload as JSON
            $payload = json_encode($body);
        // Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

            //var_dump($msg);
        // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));

            fclose($fp);

            return true;
            //print_r($err);
        }
}
