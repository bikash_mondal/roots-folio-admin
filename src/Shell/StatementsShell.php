<?php
namespace App\Shell;
use Cake\Console\Shell;
class StatementsShell extends Shell
{
    public function main()
    {
        $this->out('Hello world.');
    }
    
      public function renewnoti()
    {
        $users = $this->Users->find()->where(['datediff(Users.subscription_end,now())' => 30])->toArray();
        foreach($users as $user)
        {
            if(!empty($user['deviceid']))
            {
                if($user['devicetype'] == 'android')
                {
                    $push_message = array("title" => 'PerfectShade',"message" => 'Your subscription is expiring. Renew now.','type' => 'renew','id'=>$user['id']);
                    $this->android_push(array($user['deviceid']), $push_message);
                }
                else if($user['devicetype']=='ios')
                {
                    $this->iphone_push($user['deviceid'], 'Your subscription is expiring. Renew now.','default',array('type' => 'renew','id'=>$user['id']));
                }
            }
        }
        exit;
    }
}